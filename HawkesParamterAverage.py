import numpy

InputDirectory='/home/karmake2/InfluenceModeling/Results/Hawkes/'
sections=['US','Movies','Sports','World']
#sections=['Movies','Sports']

with open(InputDirectory+'parameters.txt','w') as outputfile:
      for section in sections:
            Parameters={}
            outputfile.write('section: '+section+'\n------------------------\n')
            with open(InputDirectory+section+'.txt','r') as inputfile:
                  for line in inputfile:
                        if '[' in line:
                              parameter=line.split(':')[0]
                              value=map(float, line.split(':')[1].replace('[','').replace(']','').split(','))
                              if parameter not in Parameters:
                                    Parameters[parameter]=[]
                              Parameters[parameter].append(value)

            for parameter in Parameters:
                  outputfile.write(parameter+': ')
                  for j in range(0,len(Parameters[parameter][0])):
                        templist=[]
                        for i in range(0,len(Parameters[parameter])):
                              templist.append(Parameters[parameter][i][j])
                        #outputfile.write(str(numpy.median(templist)))
                        outputfile.write(str(templist[0]))
                        if j <len(Parameters[parameter][0])-1:
                              outputfile.write(',')
                  outputfile.write('\n')
            outputfile.write('\n')
                        
                  
                  

