import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math
import NDCG
import RBO


Sections=['Movies','Sports','US','World']
#Sections=['Movies']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))




for section in Sections:
      Relevance={}
      PredictedNDCG=[]
      NaiveNDCG=[]
      PredictedRBO=[]
      NaiveRBO=[]

      
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensitySpecial/'+section+'/'
      ARIMADirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/ARIMA/'+section+'/'
      VARDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VAR/'+section+'/'


      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])



      Points={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        if time not in Points:
                              Points[time]={}
                        if event in Points[time]:
                              Points[time][event]+=1
                        else:
                              Points[time][event]=1

      for time in Points:
            for event in range(0,NumberOfEvents):
                  if event not in Points[time]:
                        Points[time][event]=0

      for time in Points:
            Relevance[time]={}
            for event in range(0,NumberOfEvents):
                  Relevance[time][event]=math.ceil(4*(float(Points[time][event]-min(Points[time].values()))/(max(Points[time].values())-min(Points[time].values()))))
            
                        
      onlyfiles = sorted([f for f in listdir(IntensityDirectory) if isfile(join(IntensityDirectory, f)) and ('.ints_adj' in f)])

      Intensities={}

      for myfile in onlyfiles:
            event=int(myfile.replace('.ints_adj',''))
            with open(IntensityDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        Intensities[time][event]=ints
      '''
      time=69
      print('Predicted Order\n-------------------------')
      for event in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
            print(event,Intensities[time-1][event])
      print('\n\nActual Order\n-------------------------')
      for event in sorted(Points[time],key=Points[time].get,reverse=True):
            print(event,Points[time][event])
      
      '''
      for time in range(10,int(max(Points.keys()))):
            if time not in Intensities:
                  Intensities[time]=Intensities[time-1]
      
      
      
      for time in range(TrainingEndTime,int(max(Points.keys()))):
      #for time in [1500]:
            if time in Points:
                  RankedList=[]
                  for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedList.append(Relevance[time][mytuple[0]])

                  IdealList=[]
                  for mytuple in sorted(Relevance[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealList.append(Relevance[time][mytuple[0]])

                  PredictedNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))

                  RankedListRBO=[]
                  for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                  #print(RankedListRBO)

                  IdealListRBO=[]
                  flag=0
                  index=0
                  for mytuple in sorted(Points[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealListRBO.append(mytuple[0])
                        if flag==0 and mytuple[1]==0:
                              flag=1
                              RBOK=index
                        index+=1
                  if flag==0:
                        RBOK=10

                  PredictedRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))

                  
                  previous_time=time-1
                  while previous_time not in Points:
                        previous_time=previous_time-1
                  
                  RankedList=[]
                  for mytuple in sorted(Points[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedList.append(Relevance[time][mytuple[0]])

                  NaiveNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))


                  #print(RankedListRBO)
                  #print(IdealListRBO)
                  
                  RankedListRBO=[]
                  for mytuple in sorted(Points[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                  NaiveRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))
                  
      print('Naive NDCG: '+str(sum(NaiveNDCG)/len(NaiveNDCG)))
      print('Naive RBO: '+str(sum(NaiveRBO)/len(NaiveRBO)))

      
      print('\n\nPredicted NDCG: '+str(sum(PredictedNDCG)/len(PredictedNDCG)))      
      print('Predicted RBO: '+str(sum(PredictedRBO)/len(PredictedRBO)))


      PredictedNDCG=[]
      PredictedRBO=[]


      ####'time series model'

      ARIMA_Prediction={}
      onlyfiles = sorted([f for f in listdir(ARIMADirectory) if isfile(join(ARIMADirectory, f)) and ('.arima' in f)])


      for myfile in onlyfiles:
            event=int(myfile.replace('.arima',''))
            with open(ARIMADirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        frequency=float(line.split(':')[1])
                        if time not in ARIMA_Prediction:
                              ARIMA_Prediction[time]={}
                        ARIMA_Prediction[time][event]=frequency



      for time in range(TrainingEndTime,int(max(Points.keys()))):
      #for time in [1500]:
            if time in Points:
                  RankedList=[]
                  for mytuple in sorted(ARIMA_Prediction[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedList.append(Relevance[time][mytuple[0]])

                  IdealList=[]
                  for mytuple in sorted(Relevance[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealList.append(Relevance[time][mytuple[0]])

                  PredictedNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))

                  RankedListRBO=[]
                  for mytuple in sorted(ARIMA_Prediction[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                  #print(RankedListRBO)

                  IdealListRBO=[]
                  flag=0
                  index=0
                  for mytuple in sorted(Points[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealListRBO.append(mytuple[0])
                        if flag==0 and mytuple[1]==0:
                              flag=1
                              RBOK=index
                        index+=1
                  if flag==0:
                        RBOK=10

                  PredictedRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))

                  
      print('\n\nAIRMA NDCG: '+str(sum(PredictedNDCG)/len(PredictedNDCG)))
      print('ARIMA RBO: '+str(sum(PredictedRBO)/len(PredictedRBO)))



      ####'VAR model'

      ARIMA_Prediction={}
      onlyfiles = sorted([f for f in listdir(VARDirectory) if isfile(join(VARDirectory, f)) and ('.var' in f)])


      for myfile in onlyfiles:
            event=int(myfile.replace('.var',''))
            with open(VARDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        frequency=float(line.split(':')[1])
                        if time not in ARIMA_Prediction:
                              ARIMA_Prediction[time]={}
                        ARIMA_Prediction[time][event]=frequency



      for time in range(TrainingEndTime,int(max(Points.keys()))):
      #for time in [1500]:
            if time in Points:
                  RankedList=[]
                  for mytuple in sorted(ARIMA_Prediction[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedList.append(Relevance[time][mytuple[0]])

                  IdealList=[]
                  for mytuple in sorted(Relevance[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealList.append(Relevance[time][mytuple[0]])

                  PredictedNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))

                  RankedListRBO=[]
                  for mytuple in sorted(ARIMA_Prediction[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                  #print(RankedListRBO)

                  IdealListRBO=[]
                  flag=0
                  index=0
                  for mytuple in sorted(Points[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealListRBO.append(mytuple[0])
                        if flag==0 and mytuple[1]==0:
                              flag=1
                              RBOK=index
                        index+=1
                  if flag==0:
                        RBOK=10

                  PredictedRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))

                  
      print('\n\nVAR NDCG: '+str(sum(PredictedNDCG)/len(PredictedNDCG)))
      print('VAR RBO: '+str(sum(PredictedRBO)/len(PredictedRBO)))
      



                  
