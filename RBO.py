import math

def computeRBOatK(RankedList,IdealList,K):
    #print(RankedList[0:20])
    #print('\n')
    #print(IdealList[0:20])
    fractionIntersection=0.0
    for i in range(0,K):
        RankedListSet=set(RankedList[0:i+1])
        IdealListSet=set(IdealList[0:i+1])
        fractionIntersection+=len(list(RankedListSet.intersection(IdealListSet)))/float(i+1)
        #print(RankedListSet)
        #print(IdealListSet)
        #print(str(len(list(RankedListSet.intersection(IdealListSet)))/float(i+1)))
    return fractionIntersection/float(K)      
        
    
