
import os
from os import listdir
from os.path import isfile, join
import shutil
import math
from pandas import read_csv
from pandas import datetime
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt




ARorder=5
Sections=['Movies','Sports','US','World']
#Sections=['World']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))

for section in Sections:
      ConflictMemory={}
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/ARIMA/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)



      Points={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        if time not in Points:
                              Points[time]={}
                        if event in Points[time]:
                              Points[time][event]+=1
                        else:
                              Points[time][event]=1

      for time in range(0,int(max(Points.keys()))+1):
            if time not in Points:
                  Points[time]={}
            for event in range(0,NumberOfEvents):
                  if event not in Points[time]:
                        Points[time][event]=0

      

      
      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True):
            print(event,ChampaionCount[event])'''
      


      ####'time series model'

      TimeSeries={}
      ARIMA_Prediction={}
      
      for event in range(0,NumberOfEvents):
            print('processing event: '+str(event))
            TimeSeries[event]=[]
            with open(OutputDirectory+str(event)+'.arima','w') as outputfile:
                  for time in sorted(Points):
                        TimeSeries[event].append(float(Points[time][event]))
                  train, test = TimeSeries[event][0:TrainingEndTime], TimeSeries[event][TrainingEndTime:len(TimeSeries[event])]
                  history = [x for x in train]
                  predictions = list()
                  counter=1

                  
                  model = ARIMA(train, order=(ARorder,1,0))
                  model_fit = model.fit(disp=0)
                  #print(len(model_fit.fittedvalues))

                  actualarray=[]
                  predictedarray=[]
                  for t in range(len(test)):
                        #output = model_fit.predict(start=TrainingEndTime+t, end=TrainingEndTime+t)
                        #yhat = TimeSeries[event][TrainingEndTime+t-1]+output
                        #output = model_fit.forecast()
                        output=0.0
                        for i in range(0,ARorder):
                              output+=model_fit.arparams[i]*TimeSeries[event][TrainingEndTime+t-i-1]
                        yhat =TimeSeries[event][TrainingEndTime+t-1]+output
                        #print(TrainingEndTime+t,TimeSeries[event][TrainingEndTime+t],yhat,output)
                        actualarray.append(TimeSeries[event][TrainingEndTime+t])
                        predictedarray.append(yhat)
                        
                        predictions.append(yhat)
                        obs = test[t]
                        history.append(obs)
                        outputfile.write(str(int(t+TrainingEndTime))+': '+str(((float(yhat))))+'\n')
                        outputfile.flush()
                        if counter%200==0:
                              print(counter)
                        counter+=1

            #plt.plot(actualarray)
            #plt.plot(predictedarray)
            #plt.show()
            
                                                                     
                                

      
                  
                        
            

      
      
