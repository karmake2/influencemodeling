import numpy
import os
from os import listdir
from os.path import isfile, join
import shutil
import math
from pandas import read_csv
from pandas import datetime
from statsmodels.tsa.api import VAR, DynamicVAR
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
#import matplotlib.pyplot as plt




VARorder=10
Sections=['Sports','Movies','US','World']
#Sections=['US']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))

for section in Sections:
      ConflictMemory={}
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VAR/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)



      Points={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        if time not in Points:
                              Points[time]={}
                        if event in Points[time]:
                              Points[time][event]+=1
                        else:
                              Points[time][event]=1

      for time in range(0,int(max(Points.keys()))+1):
            if time not in Points:
                  Points[time]={}
            for event in range(0,NumberOfEvents):
                  if event not in Points[time]:
                        Points[time][event]=0

      

      
      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True):
            print(event,ChampaionCount[event])'''
      


      ####'time series model'

      TimeSeries=[]
      ARIMA_Prediction={}
      train=[]
      test=[]
      
      for event in range(0,NumberOfEvents):
            #print('processing event: '+str(event))
            TimeSeries.append([])
            train.append([])
            test.append([])
            
            for time in sorted(Points):
                  TimeSeries[event].append(float(Points[time][event]))
                  train[event] = TimeSeries[event][0:TrainingEndTime]
                  test[event]= TimeSeries[event][TrainingEndTime:len(TimeSeries[event])]

      train=numpy.transpose(train)
      test=numpy.transpose(test)
      TimeSeries=numpy.transpose(TimeSeries)
      
      model = VAR(train)
      model_fit = model.fit(VARorder)
      lag_order = model_fit.k_ar
      #print(model_fit.params)
      #break

      '''output=[]
      for t in range(len(test)):
            #output.append(model_fit.forecast(TimeSeries[-lag_order+TrainingEndTime+t:],1))
            #print(output[t])
            #print(-lag_order+TrainingEndTime+t, output)
            output.append([])
            for event in range(0,NumberOfEvents):
                  mysum=0.0
                  for i in range(0,VARorder):
                        for subevent in range(0,NumberOfEvents):
                              mysum+=model_fit.params[event*VARorder+i][subevent]*TimeSeries[TrainingEndTime+t-i-1][event]
                  mysum+=model_fit.params[VARorder*NumberOfEvents][event]
                  output[t].append(mysum)
            print(str(output[t])+'\n\n')
            break
            #train=numpy.append(train,[test[t]],axis=0)
            #model = VAR(train)
            #model_fit = model.fit(VARorder)
            if t%100==0:
                  print(t)
            #print(len(train))'''

      output=[]
      for t in range(len(test)):
            #print((model_fit.forecast(TimeSeries[-lag_order+TrainingEndTime+t:],1)))
            #print(output[t])
            #print(-lag_order+TrainingEndTime+t, output)
            
            output.append([])
            for event in range(0,NumberOfEvents):
                  mysum=model_fit.params[0][event]
                  for i in range(0,VARorder):
                        mysum+=numpy.dot(model_fit.params[i*NumberOfEvents+event+1],TimeSeries[TrainingEndTime+t-i-1])
                  output[t].append(mysum)
            #train=numpy.append(train,[test[t]],axis=0)
            #model = VAR(train)
            #model_fit = model.fit(VARorder)
            if t%100==0:
                  print(t)
            #print(len(train))
            #print(str(output)+'\n\n')
                  
      
      for event in range(0,NumberOfEvents):
            with open(OutputDirectory+str(event)+'.var','w') as outputfile:
                  for t in range(len(test)):
                        outputfile.write(str(int(t+TrainingEndTime))+': '+str(((float(output[t][event]))))+'\n')

            
      '''actualarray=[]
      predictedarray=[]
      for t in range(len(test)):
            #output = model_fit.predict(start=TrainingEndTime+t, end=TrainingEndTime+t)
            #yhat = TimeSeries[event][TrainingEndTime+t-1]+output
            #output = model_fit.forecast()
            output=0.0
            for i in range(0,ARorder):
                  output+=model_fit.arparams[i]*TimeSeries[event][TrainingEndTime+t-ARorder+i]
            yhat =output
            #print(TrainingEndTime+t,TimeSeries[event][TrainingEndTime+t],yhat)
            actualarray.append(TimeSeries[event][TrainingEndTime+t])
            predictedarray.append(yhat)
            
            predictions.append(yhat)
            obs = test[t]
            history.append(obs)
            outputfile.write(str(int(t+TrainingEndTime))+': '+str(int(round(float(yhat))))+'\n')
            outputfile.flush()
            if counter%100==0:
                  print(counter)
            counter+=1

            #plt.plot(actualarray)
            #plt.plot(predictedarray)
            #plt.show()'''
            
                                                                     
                                

      
                  
                        
            

      
      
