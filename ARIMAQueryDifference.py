import sys
import os
from os import listdir
from os.path import isfile, join
import shutil
import math
from pandas import read_csv
from pandas import datetime
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error


ARorder=5
Sections=['Movies','Sports','US','World']
#Sections=['Movies']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))




for section in Sections:
      ConflictMemory={}
      InputDirectory='/home/karmake2/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/home/karmake2/InfluenceModeling/Results/IntensityQuerySpecial/'+section+'/'
      OutputDirectory='/home/karmake2/InfluenceModeling/Results/ARIMAQueryDiff/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)
      AllQueries={}

      QueryCounts={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        actualTime=float(line.split(':')[0])
                        if actualTime in ConflictMemory:
                              continue
                        else:
                              ConflictMemory[actualTime]=1
                        query=line.strip().split(':')[2]
                        if time not in QueryCounts:
                              QueryCounts[time]={}
                        if query in QueryCounts[time]:
                              QueryCounts[time][query]+=1
                        else:
                              QueryCounts[time][query]=1

                        if query not in AllQueries:
                              AllQueries[query]=1


      for time in range(0,int(max(QueryCounts.keys()))+1):
            if time not in QueryCounts:
                  QueryCounts[time]={}
            for query in AllQueries.keys():
                  if query not in QueryCounts[time]:
                        QueryCounts[time][query]=0

      

      
      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True):
            print(event,ChampaionCount[event])'''
      


      ####'time series model'

      TimeSeries={}
      ARIMA_Prediction={}

      queryNumber=0
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      print('\n\nTotal Queries: '+str(len(AllQueries.keys()))+'\n--------------------------\n')
      
      for query in AllQueries.keys():
            queryNumber+=1
            print(str(queryNumber)+' :processing query: '+str(query))
            TimeSeries[query]=[]
            with open(OutputDirectory+str(query)+'.arima','w') as outputfile:
                  for time in sorted(QueryCounts):
                        TimeSeries[query].append(float(QueryCounts[time][query]))
                  train, test = TimeSeries[query][0:TrainingEndTime], TimeSeries[query][TrainingEndTime:len(TimeSeries[query])]
                  counter=1

                  model = ARIMA(train,order=(ARorder,1,0))
                  try:
                        model_fit = model.fit(disp=0)
                        for t in range(len(test)):
                              output=0.0
                              for i in range(0,ARorder):
                                    output+=model_fit.arparams[i]*TimeSeries[query][TrainingEndTime+t-i-1]
                              yhat=TimeSeries[query][TrainingEndTime+t-1]+output
                              outputfile.write(str(int(t+TrainingEndTime))+': '+str(((float(yhat))))+'\n')
                              counter+=1
                  except:
                        for t in range(len(test)):
                              outputfile.write(str(int(t+TrainingEndTime))+': 0.0\n')
                              counter+=1
                        
                        


                  
                                

      
                  
                        
            

      
      
