import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math
import NDCG
import RBO


Sections=['Movies','Sports','US','World']
#Sections=['World']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))




for section in Sections:
      Relevance={}
      PredictedNDCG=[]
      NaiveNDCG=[]
      PredictedRBO=[]
      NaiveRBO=[]


      
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensityQuerySpecial/'+section+'/'
      VARDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VARQuery/'+section+'/'
      ARIMADirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/ARIMAQueryDiff/'+section+'/'
      
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])



      AllQueries={}

      QueryCounts={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        query=line.strip().split(':')[2]
                        if time not in QueryCounts:
                              QueryCounts[time]={}
                        if query in QueryCounts[time]:
                              QueryCounts[time][query]+=1
                        else:
                              QueryCounts[time][query]=1

                        if query not in AllQueries:
                              AllQueries[query]=1

      for time in QueryCounts:
            for query in AllQueries.keys():
                  if query not in QueryCounts[time]:
                        QueryCounts[time][query]=0

      for time in QueryCounts:
            Relevance[time]={}
            for query in QueryCounts[time]:
                  Relevance[time][query]=4*math.ceil(float(QueryCounts[time][query])/max(QueryCounts[time].values()))
            
                        
      onlyfiles = sorted([f for f in listdir(IntensityDirectory) if isfile(join(IntensityDirectory, f)) and ('.ints_adj' in f)])

      Intensities={}

      for myfile in onlyfiles:
            event=int(myfile.replace('.ints_adj',''))
            with open(IntensityDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        query=line.split(' ## ')[0]
                        intensitiesWithTime=line.split(' ## ')[1].split(',')
                        for insTime in intensitiesWithTime:
                              time=int(float(insTime.split(':')[0]))
                              ints=float(insTime.split(':')[1])
                              if time not in Intensities:
                                    Intensities[time]={}
                              if query not in Intensities[time]:
                                    Intensities[time][query]=ints
                              else:
                                    Intensities[time][query]+=ints
      '''
      time=69
      print('Predicted Order\n-------------------------')
      for event in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
            print(event,Intensities[time-1][event])
      print('\n\nActual Order\n-------------------------')
      for event in sorted(Points[time],key=Points[time].get,reverse=True):
            print(event,Points[time][event])
      
      '''
      
      
      
      for time in range(TrainingEndTime,int(max(QueryCounts.keys()))):
            if time in QueryCounts:
                  RankedList=[]
                  for query in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
                        RankedList.append(Relevance[time][query])

                  IdealList=[]
                  for query in sorted(Relevance[time],key=Relevance[time].get,reverse=True):
                        IdealList.append(Relevance[time][query])

                  PredictedNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))

                  RankedListRBO=[]
                  for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                        
                  IdealListRBO=[]
                  flag=0
                  index=0
                  for mytuple in sorted(QueryCounts[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealListRBO.append(mytuple[0])
                        if flag==0 and mytuple[1]==0:
                              flag=1
                              RBOK=index
                        index+=1
                  if flag==0:
                        RBOK=10

                  PredictedRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))

                  previous_time=time-1
                  while previous_time not in QueryCounts:
                        previous_time=previous_time-1
                  
                  RankedList=[]
                  for query in sorted(QueryCounts[previous_time],key=QueryCounts[previous_time].get,reverse=True):
                        RankedList.append(Relevance[time][query])

                  NaiveNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))


                  RankedListRBO=[]
                  for mytuple in sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                  NaiveRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))

      print('Naive NDCG: '+str(sum(NaiveNDCG)/len(NaiveNDCG)))                 
      print('Predicted NDCG: '+str(sum(PredictedNDCG)/len(PredictedNDCG)))
      print('\n\nNaive RBO: '+str(sum(NaiveRBO)/len(NaiveRBO)))
      print('Predicted RBO: '+str(sum(PredictedRBO)/len(PredictedRBO)))







      ####ARIMA#####
      onlyfiles = sorted([f for f in listdir(ARIMADirectory) if isfile(join(ARIMADirectory, f)) and ('.arima' in f)])

      Intensities={}

      for myfile in onlyfiles:
            query=(myfile.replace('.arima',''))
            with open(ARIMADirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        if query not in Intensities[time]:
                              Intensities[time][query]=ints
                        else:
                              Intensities[time][query]+=ints
      '''
      time=69
      print('Predicted Order\n-------------------------')
      for event in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
            print(event,Intensities[time-1][event])
      print('\n\nActual Order\n-------------------------')
      for event in sorted(Points[time],key=Points[time].get,reverse=True):
            print(event,Points[time][event])
      
      '''
      
      
      
      for time in range(TrainingEndTime+1,int(max(QueryCounts.keys()))):
            if time in QueryCounts:
                  RankedList=[]
                  for query in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
                        RankedList.append(Relevance[time][query])

                  IdealList=[]
                  for query in sorted(Relevance[time],key=Relevance[time].get,reverse=True):
                        IdealList.append(Relevance[time][query])

                  PredictedNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))

                  RankedListRBO=[]
                  for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                        
                  IdealListRBO=[]
                  flag=0
                  index=0
                  for mytuple in sorted(QueryCounts[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealListRBO.append(mytuple[0])
                        if flag==0 and mytuple[1]==0:
                              flag=1
                              RBOK=index
                        index+=1
                  if flag==0:
                        RBOK=10

                  PredictedRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))

                  previous_time=time-1
                  while previous_time not in QueryCounts:
                        previous_time=previous_time-1
                  
                  RankedList=[]
                  for query in sorted(QueryCounts[previous_time],key=QueryCounts[previous_time].get,reverse=True):
                        RankedList.append(Relevance[time][query])

                  NaiveNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))


                  RankedListRBO=[]
                  for mytuple in sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                  NaiveRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))
                
      print('\n\nARIMA NDCG: '+str(sum(PredictedNDCG)/len(PredictedNDCG)))
      print('ARIMA RBO: '+str(sum(PredictedRBO)/len(PredictedRBO)))




      

      ####VAR#####
      onlyfiles = sorted([f for f in listdir(VARDirectory) if isfile(join(VARDirectory, f)) and ('.var' in f)])

      Intensities={}

      for myfile in onlyfiles:
            query=(myfile.replace('.var',''))
            with open(VARDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        if query not in Intensities[time]:
                              Intensities[time][query]=ints
                        else:
                              Intensities[time][query]+=ints
      '''
      time=69
      print('Predicted Order\n-------------------------')
      for event in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
            print(event,Intensities[time-1][event])
      print('\n\nActual Order\n-------------------------')
      for event in sorted(Points[time],key=Points[time].get,reverse=True):
            print(event,Points[time][event])
      
      '''
      
      
      
      for time in range(TrainingEndTime+1,int(max(QueryCounts.keys()))):
            if time in QueryCounts:
                  RankedList=[]
                  for query in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
                        RankedList.append(Relevance[time][query])

                  IdealList=[]
                  for query in sorted(Relevance[time],key=Relevance[time].get,reverse=True):
                        IdealList.append(Relevance[time][query])

                  PredictedNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))

                  RankedListRBO=[]
                  for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                        
                  IdealListRBO=[]
                  flag=0
                  index=0
                  for mytuple in sorted(QueryCounts[time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        IdealListRBO.append(mytuple[0])
                        if flag==0 and mytuple[1]==0:
                              flag=1
                              RBOK=index
                        index+=1
                  if flag==0:
                        RBOK=10

                  PredictedRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))

                  previous_time=time-1
                  while previous_time not in QueryCounts:
                        previous_time=previous_time-1
                  
                  RankedList=[]
                  for query in sorted(QueryCounts[previous_time],key=QueryCounts[previous_time].get,reverse=True):
                        RankedList.append(Relevance[time][query])

                  NaiveNDCG.append(NDCG.computeNDCGatK(RankedList,IdealList,10))


                  RankedListRBO=[]
                  for mytuple in sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                        RankedListRBO.append(mytuple[0])
                  NaiveRBO.append(RBO.computeRBOatK(RankedListRBO,IdealListRBO,RBOK))
                
      print('\n\nVAR NDCG: '+str(sum(PredictedNDCG)/len(PredictedNDCG)))
      print('VAR RBO: '+str(sum(PredictedRBO)/len(PredictedRBO)))
      



                  
