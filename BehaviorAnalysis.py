import math
import os
from os import listdir
from os.path import isfile, join
import shutil


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime
import numpy


StartTime=1459436400
EndTime=1470978000
EndDay=int(round(((float) (EndTime) - (float)(StartTime))/(60*60)))


InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData'
OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/Plots/Frequency'

Sections=['World','Movies','US','Sports']

PeakGlobalMax=0



            

try:
      shutil.rmtree(OutputDirectory)
except:
      print ('No Folder Found')
os.makedirs(OutputDirectory)


PeakHistogram={}


for section in Sections:
      os.makedirs(OutputDirectory+'/'+section)
      print ('\n\nProcessing: '+section+'\n************************\n\n')
      PeakHistogram[section]={}
            
      onlyfiles = [f for f in listdir(InputDirectory+'/'+section) if isfile(join(InputDirectory+'/'+section, f)) and ('.csv' in f)]

      
      for myfile in onlyfiles:
            with open (InputDirectory+'/'+section+'/'+myfile,'r') as inputfile:
                  url=inputfile.readline().strip()
                  inputfile.readline()
                  newstimestamp=inputfile.readline()
                  newsDay=round(((float) (newstimestamp) - (float)(StartTime))/(60*60)) 
                  inputfile.readline()
                  newsarticle=inputfile.readline()

                  NewsNGrams={}

                  while True:
                        myline=inputfile.readline()
                        if '##################' in myline:
                              inputfile.readline()
                              inputfile.readline()
                              break

                  while True:
                        myline=inputfile.readline()
                        if myline.strip()=='':
                              break
                        NewsNGrams[myline.split(',')[0]]=float(myline.split(',')[1])

                  while True:
                        myline=inputfile.readline()
                        if 'Queries' in myline:
                              inputfile.readline()
                              inputfile.readline()
                              break


                  UniqueQueriesSimilarity={}
                  UniqueQueriesCount={}
                  UniqueQueriesTimelag={}

                  while True:
                        myline=inputfile.readline()
                        if myline.strip()=='':
                              break
                        
                        if float(myline.split(' #&# ')[1].strip()) < 1.25:
                              break
                        
                        
                        if myline.split(' #&# ')[0] not in UniqueQueriesSimilarity:
                              UniqueQueriesSimilarity[myline.split(' #&# ')[0]]=float(myline.split(' #&# ')[1])

                        if myline.split(' #&# ')[0] not in UniqueQueriesCount:
                              UniqueQueriesCount[myline.split(' #&# ')[0]]=1
                        else:
                              UniqueQueriesCount[myline.split(' #&# ')[0]]+=1


                        querytimestamp=int(myline.split(' #&# ')[2])
                        if myline.split(' #&# ')[0] not in UniqueQueriesTimelag:
                              UniqueQueriesTimelag[myline.split(' #&# ')[0]]=[]
                        TimeLag=round(((float) (querytimestamp) - (float)(StartTime))/(60*60)) 
                        UniqueQueriesTimelag[myline.split(' #&# ')[0]].append(TimeLag)
                  


                  QueryRelevance={}


                  UnigramCount={}
                  UnigramCountPerDay={}

                  UniqueQueryList=sorted(UniqueQueriesSimilarity, key=UniqueQueriesSimilarity.get, reverse=True)[:]
                  #MaxSimilarityDifference=UniqueQueriesSimilarityCount[UniqueQueryList[0]]-UniqueQueriesSimilarityCount[UniqueQueryList[1]]
                  ThresholdIndex=0
                  
                  
                  for i in range(0,len(UniqueQueryList)):
                        if UniqueQueriesSimilarity[UniqueQueryList[i]] >= 1.0:
                              #MaxSimilarityDifference=UniqueQueriesSimilarityCount[UniqueQueryList[i]]-UniqueQueriesSimilarityCount[UniqueQueryList[i+1]]
                              ThresholdIndex=i+1
      

                  
                  for myquery in UniqueQueryList[:ThresholdIndex]:
                        for word in myquery.split():
                              if word in UnigramCount:
                                    UnigramCount[word]+=UniqueQueriesCount[myquery]
                              else:
                                    UnigramCount[word]=UniqueQueriesCount[myquery]

                              
                              if word not in UnigramCountPerDay:
                                    UnigramCountPerDay[word]={}
                              
                              
                              for TimeLag in UniqueQueriesTimelag[myquery]:
                                    if TimeLag in UnigramCountPerDay[word]:
                                          UnigramCountPerDay[word][TimeLag]+=1
                                    else:
                                          UnigramCountPerDay[word][TimeLag]=1


                        for TimeLag in UniqueQueriesTimelag[myquery]:
                              if TimeLag in QueryRelevance:
                                    QueryRelevance[TimeLag]+=UniqueQueriesSimilarity[myquery]
                              else:
                                    QueryRelevance[TimeLag]=UniqueQueriesSimilarity[myquery]

                        


                                    
                  for word in UnigramCountPerDay:
                        for daylag in range(int(min(UnigramCountPerDay[word].keys())),int(max(UnigramCountPerDay[word].keys()))+1):
                              if daylag not in UnigramCountPerDay[word]:
                                    UnigramCountPerDay[word][daylag]=0


                  for daylag in range(0,EndDay):
                        if daylag not in QueryRelevance:
                              QueryRelevance[daylag]=0
                  
                  
                  similarityList=[]

                  for TimeLag in sorted(QueryRelevance):
                        similarityList.append(QueryRelevance[TimeLag])


                  if len(similarityList)==0:
                        continue
                  
                  

 
                  plt.figure()
                  plt.plot(sorted(QueryRelevance.keys()), similarityList)
                  plt.axvline(newsDay,color='r')
                  plt.title('Time of News Release: '+datetime.datetime.fromtimestamp(int(newstimestamp)).strftime('%Y-%m-%d %H:%M:%S'))
                  plt.xlabel("Time (in Days)")
                  plt.ylabel("Relevant Query Frequency")
                  axes = plt.gca()
                  axes.set_xlim([0,EndDay])
                  #axes.set_ylim([0,50])
                  
                  
                  plt.savefig(OutputDirectory+'/'+section+'/'+myfile.replace('.csv','.similarityDistribution.png'))
                  print('processed News ID: ' + myfile.replace('.csv',''))
                  plt.close()



                  plt.figure()
                  ax = plt.subplot(111)
                  ax.axvline(0,color='r')
                  
                  for word in sorted(UnigramCount, key=UnigramCount.get, reverse=True)[:10]:
                        counts=[]
                        for day in sorted(list(UnigramCountPerDay[word].keys())):
                              counts.append(UnigramCountPerDay[word][day])
                        ax.plot(sorted(list(UnigramCountPerDay[word].keys())), counts,label=word)
                        # Shrink current axis's height by 10% on the bottom
                  box = ax.get_position()
                  ax.set_position([box.x0, box.y0 + box.height * 0.1,
                                   box.width, box.height * 0.9])

                  # Put a legend below current axis
                  ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),fancybox=True, shadow=True, ncol=5)
                  #plt.xticks(range(len(Good)), list(Good.keys()))
                  plt.title('NewsID: '+str(myfile.replace('.csv','')))
                  plt.xlabel("Day")
                  plt.ylabel("Frequency")
                  axes = plt.gca()
                  plt.close()

