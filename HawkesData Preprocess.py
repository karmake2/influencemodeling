import math
import os
from os import listdir
from os.path import isfile, join
import shutil


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime
import numpy
import random


StartTime=1459436400
DataDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData'

Sections=['Movies','Sports','US','World']

#Sections=['Movies']

for section in Sections:
      try:
            os.system("rm "+DataDirectory+'/'+section+'/*.hawkesData')
      except:
            print('No hawkesData file found')
      print ('\n\nProcessing: '+section+'\n************************\n\n')
            
      onlyfiles = [f for f in listdir(DataDirectory+'/'+section) if isfile(join(DataDirectory+'/'+section, f)) and ('.csv' in f)]
      #onlyfiles=['0.csv','1.csv']
      for myfile in onlyfiles:
            with open (DataDirectory+'/'+section+'/'+myfile,'r') as inputfile:
                  url=inputfile.readline().strip()
                  inputfile.readline()
                  newstimestamp=inputfile.readline().strip()
                  inputfile.readline()
                  newsarticle=inputfile.readline()

                  NewsNGrams={}

                  while True:
                        myline=inputfile.readline()
                        if '##################' in myline:
                              inputfile.readline()
                              inputfile.readline()
                              break

                  while True:
                        myline=inputfile.readline()
                        if myline.strip()=='':
                              break
                        NewsNGrams[myline.split(',')[0]]=float(myline.split(',')[1])

                  while True:
                        myline=inputfile.readline()
                        if 'Queries' in myline:
                              inputfile.readline()
                              inputfile.readline()
                              break


                  Queries={}
                  maxSimilarity=0.0
                  DayActivity={}
                  
                  while True:
                        myline=inputfile.readline().strip()
                        if myline=='':
                              break
                        similarity=float(myline.split(' #&# ')[1].strip())
                        if similarity>maxSimilarity:
                              maxSimilarity=similarity
                        
                        timestamp=int(myline.split(' #&# ')[2])-StartTime
                        if (timestamp) not in Queries:
                              Queries[(timestamp)]=[]
                        Queries[(timestamp)].append(myline.split(' #&# ')[1]+':'+myline.split(' #&# ')[0])

                        daylag=int(float((timestamp)-int(newstimestamp))/(24*60*60))
                        if daylag not in DayActivity:
                              DayActivity[daylag]=0
                        DayActivity[daylag]+=similarity
                  

                  if len(DayActivity)==0:
                        maxSimilarity=1.0
                        continue
                        
                        
                  '''
                  maximum = max(DayActivity, key=DayActivity.get)

                  
                  if DayActivity[maximum] < 200:
                        continue
                  '''
                  
                  
                  
                  
                  
                  
                  
                        
      
                  #SampledQueries=sorted(random.sample(Queries.keys(),min(10000,len(Queries))))
                  #SampledQueries.append(int(newstimestamp))
                  

##                  if int(newstimestamp) not in Queries:
##                        Queries[int(newstimestamp)]=[]
##                  Queries[int(newstimestamp)].append(str(maxSimilarity*10.0)+':Actual News Article')
##                

                  queryCounter=0
                  duplicateCounter=0
                  print 'Processing: '+ myfile
                  with open (DataDirectory+'/'+section+'/'+myfile.replace('.csv','.hawkesData'),'w') as outputfile:
                        sortedQueries=sorted(Queries)
                        for time in sortedQueries:
                              queryList=list((Queries[time]))
                              if len(queryList)>1:
                                    duplicateCounter+=len(queryList)
                                    #print(str(time)+' ## '+str(queryList))
                              outputfile.write(str((float(time)/(60*60)))+':'+queryList[0]+'\n')
                              queryCounter+=len(queryList)
                  print(queryCounter,duplicateCounter)

                              
                        
 
