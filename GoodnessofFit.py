
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math



Sections=['Movies','Sports','US','World']
#Sections=['US']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))

for section in Sections:
      ConflictMemory={}
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensitySpecial/'+section+'/'
      ARIMADirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/ARIMA/'+section+'/'
      VARDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VAR/'+section+'/'



      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)



      Points={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        if time not in Points:
                              Points[time]={}
                        if event in Points[time]:
                              Points[time][event]+=1
                        else:
                              Points[time][event]=1

      for time in Points:
            for event in range(0,NumberOfEvents):
                  if event not in Points[time]:
                        Points[time][event]=0
                        
      onlyfiles = sorted([f for f in listdir(IntensityDirectory) if isfile(join(IntensityDirectory, f)) and ('.ints_adj' in f)])

      Intensities={}

      for myfile in onlyfiles:
            event=int(myfile.replace('.ints_adj',''))
            with open(IntensityDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        Intensities[time][event]=ints


      event1Actual=[]
      event2Actual=[]
      event1Simulation=[]
      event2Simulation=[]
      timearray=[]
      
      for time in sorted(Points.keys()):
            if time >1500 and time<1700:
                  event1Actual.append(Points[time][0])
                  event2Actual.append(Points[time][1])
                  event1Simulation.append(Intensities[time][0])
                  event2Simulation.append(Intensities[time][1])
                  timearray.append(time)


      ax = plt.gca()

      actualPlot=plt.plot(timearray,event1Actual,label='Actual Frequency',marker="*",color='brown')
      simlationPlot=plt.plot(timearray,event1Simulation,label='Simulated Intensity',marker="o",color='blue')
      plt.ylabel('Frequency/Influence')
      plt.xlabel('time slot (in hours)')
      #plt.plot(timearray,event1Simulation)
      #plt.plot(timearray,event2Simulation)
      plt.legend([actualPlot, simlationPlot], ['Actual', 'Simulation'])
      handles, labels = ax.get_legend_handles_labels()
      ax.legend(handles, labels)
      plt.show()
      break
            




      
