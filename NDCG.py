import math

def computeNDCGatK(RankedList,IdealList,K):
    DCG=0.0
    for i in range(0,K):
        DCG+=RankedList[i]/math.log(i+2,2)

    IDCG=0.0
    for i in range(0,K):
        IDCG+=IdealList[i]/math.log(i+2,2)

    #print(DCG,IDCG)
    return DCG/IDCG
        
    
        
    
