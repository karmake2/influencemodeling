import numpy as np
from scipy.optimize import minimize
import math
import numpy
import random
import os
from os import listdir
from os.path import isfile, join
import shutil
import traceback
import sys
from numpy import linalg as LA
import ast
import json

'''
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
'''

OutputDirectory='/home/karmake2/InfluenceModeling/Results/Hawkes/'

'''
try:
    shutil.rmtree(OutputDirectory)
except:
    print ('No Folder Found')
os.makedirs(OutputDirectory)
'''
      
MaxSimilarity=0
starttime=1459486800
endtime=(1470978000-starttime)/(60*60)

TrainingEndTime=(1464652800-starttime)/(60*60)


def drange2(start, stop, step):
    numelements = int((stop-start)/float(step))
    for i in range(numelements+1):
            yield start+i*step




'''           
def lambda_s(lambda_0,s,points,similarities,alpha,beta):
      lambda_s=lambda_0
      for i in range(0,len(points)):
            if points[i] < s:
                  lambda_s+=alpha*similarities[k]*math.exp(-(beta)*(s-points[i]))
      return lambda_s



def CorpusLoglikelihoodHawkes(parameters):
    likelihood=1
    onlyfiles = [f for f in listdir(ResultDirectory) if isfile(join(ResultDirectory, f)) and ('.hawkesData' in f)]

    for myfile in onlyfiles:
        points=[]
        similarities=[]
        with open(ResultDirectory+myfile,'r') as inputfile:
            for line in inputfile:
                if float(line.strip().split(':')[1]) < 1.0:
                      continue
                points.append(float(line.split(':')[0]))
                similarities.append(float(line.split(':')[1]))
        for k in range(0,len(points)):
            points[k]=(points[k]-starttime)/(60*60)
        
        likelihood+=LoglikelihoodHawkes(parameters,points,similarities)
    
    print parameters,likelihood
    return -likelihood
            





def LoglikelihoodHawkes(parameters,points,similarities):
    lambda_0=parameters[0]
    alpha=parameters[1]
    beta=parameters[2]


    likelihood=points[len(points)-1]-lambda_0*points[len(points)-1]
    for i in range (0,len(points)):
        likelihood-=(similarities[i])*(alpha/beta)*(1-math.exp(-(beta)*(points[len(points)-1]-points[i])))   
    for i in range(0,len(points)):
        term=lambda_0
        for k in range(max(0,i-50),i):
              term+=alpha*similarities[k]*math.exp(-(beta)*(points[i]-points[k]))
        try:
              likelihood+=math.log(term)
        except:
              print lambda_0,alpha,beta
    return likelihood/1000








lambda_0=[]
alpha=[]
beta=[]
gamma=[]
'''
      
def LoglikelihoodHawkes(parameters):
    #print(parameters)
    etas=[]
    alphas=[]
    neus=[]
    ro=[]
    mu=[]
    phi=[]
    shi=[]

    k=0
    for i in range(0,NumberOfEvents):
        etas.append(parameters[k])
        k+=1
    for i in range(0,NumberOfEvents):
        alphas.append(parameters[k])
        k+=1
    for i in range(0,NumberOfEvents):
        neus.append([])
        for j in range(0,NumberOfEvents):
            neus[i].append(parameters[k])
            k+=1
    for i in range(0,NumberOfEvents):
        ro.append(parameters[k])
        k+=1
    for i in range(0,NumberOfEvents):
        mu.append(parameters[k])
        k+=1
    for i in range(0,NumberOfEvents):
        phi.append(parameters[k])
        k+=1
    for i in range(0,NumberOfEvents):
        shi.append(parameters[k])
        k+=1
    Likelihood = computeIntensity(etas,alphas,neus,ro,mu,phi,shi)+computeMark(ro,mu,phi,shi)-computeCompensator(etas,alphas,neus,ro,mu,phi,shi) - math.pow(LA.norm(parameters),2)    
    #Likelihood = computeMark(ro,mu,phi,shi)-math.pow(LA.norm(parameters),2) 
    #print computeIntensity(etas,alphas,neus,ro,mu,phi,shi),computeMark(ro,mu,phi,shi),computeCompensator(etas,alphas,neus,ro,mu,phi,shi), math.pow(LA.norm(parameters),2) 
    print(Likelihood)
    return -Likelihood
    



def computeIntensity(etas,alphas,neus,ro,mu,phi,shi):
    lambdas=[[0 for x in range(len(PointList))] for y in range(len(etas))]
    for event in range(NumberOfEvents):
        lambdas[event][0]=etas[event]
    #print(lambdas)
        
    for i in range(1,len(PointList)):
        for j in range(0,len(etas)):
            previousEvent=Points[PointList[i-1]]['event']
            previousMark=(Points[PointList[i-1]]['similarity']*shi[previousEvent]+phi[previousEvent])*(float((ro[previousEvent]-1)*(ro[previousEvent]-2))/((phi[previousEvent]*(ro[previousEvent]-1)*(ro[previousEvent]-2)+shi[previousEvent]*mu[previousEvent]*(ro[previousEvent]-2))))
            #previousMark=1
            lambdas[j][i]=etas[j]+math.exp(-alphas[0]*(PointList[i]-PointList[i-1]))*(lambdas[j][i-1]-etas[j])+neus[j][previousEvent]*alphas[0]*math.exp(-alphas[0]*(PointList[i]-PointList[i-1]))*previousMark
    #print(lambdas)
    Intensity=0
    for i in range(0,len(PointList)):
        Intensity+=math.log(lambdas[Points[PointList[i]]['event']][i])
        #print Points[PointList[i]]['event'],lambdas[Points[PointList[i]]['event']][i]
    return Intensity






def computeMark(ro,mu,phi,shi):
    MarkScore=0
    for time in PointList:
        event=Points[time]['event']
        try:
            #print(ro)
            #MarkScore+=math.log((ro[event]*math.pow(mu[event],ro[event]))/(math.pow((Points[time]['similarity']+mu[event]),(ro[event]+1))))
            MarkScore+=math.log(ro[event]) + ro[event]*math.log(mu[event]) - (ro[event]+1)*math.log(Points[time]['similarity']+mu[event])
            #MarkScore=1
        except:
            print(ro)
            sys.exit()
    return MarkScore





def computeCompensator(etas,alphas,neus,ro,mu,phi,shi):
    compensatorScore=[]
    for j in range(0,len(etas)):
        compensatorScore.append(etas[j]*PointList[len(PointList)-1])
        for i in range(0,len(PointList)):
            event=Points[PointList[i]]['event']
            Mark=(Points[PointList[i]]['similarity']*shi[event]+phi[event])*(float((ro[event]-1)*(ro[event]-2))/((phi[event]*(ro[event]-1)*(ro[event]-2)+shi[event]*mu[event]*(ro[event]-2))))
            compensatorScore[j]+=neus[j][event]*Mark*(1-math.exp(-alphas[0]*(PointList[len(PointList)-1]-PointList[i])))
    return sum(compensatorScore)
       





for section in ['World','Movies','Sports','US']:
    InputDirectory='/home/karmake2/InfluenceModeling/Data/BaseData/'+section+'/'
    
    with open(OutputDirectory+section+'.txt','w') as outputfile:
        for iteration in range(0,5):
            print 'iteration: '+str(iteration)
            
            onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])
            #onlyfiles=['0.hawkesData','1.hawkesData']
            
            EventPoints={}
            Points={}
            NumberOfEvents=len(onlyfiles)
            
            for myfile in onlyfiles:
                EventPoints[myfile.replace('.hawkesData','')]={}
                with open(InputDirectory+myfile,'r') as inputfile:
                    for line in inputfile:
                        time=float(line.split(':')[0])
                        if time > TrainingEndTime:
                            break
                        if time in Points:
                            continue
                        '''actualTime=float(line.split(':')[0])
                        while time in Points:
                            noise=random.uniform(-0.01,0.01)
                            time=actualTime+noise'''
                        similarity=float(line.split(':')[1])
                        query=line.split(':')[2].strip()
                        EventPoints[myfile.replace('.hawkesData','')][time]={}
                        EventPoints[myfile.replace('.hawkesData','')][time]['query']=query
                        EventPoints[myfile.replace('.hawkesData','')][time]['similarity']=similarity-1.25
                        Points[time]={}
                        Points[time]['event']=int(myfile.replace('.hawkesData',''))
                        Points[time]['query']=query
                        Points[time]['similarity']=similarity-1.25


            etas=[]
            alphas=[]
            neus=[]
            ro=[]
            mu=[]
            phi=[]
            shi=[]
            PointList=sorted(Points.keys())

            for i in range(0,NumberOfEvents):
                etas.append(random.uniform(0.001, 10))
                alphas.append(random.uniform(0.001, 10))
                ro.append(random.uniform(2.001, 10))
                mu.append(random.uniform(0.001, 10))
                phi.append(random.uniform(0.001, 10))
                shi.append(random.uniform(0.001, 10))
                neus.append([])
                for j in range(0,NumberOfEvents):
                    neus[i].append(random.uniform(0.001,1))

        ##    for i in range(0,NumberOfEvents):
        ##        etas.append(0.001)
        ##        alphas.append(0.001)
        ##        ro.append(2.1)
        ##        mu.append(0.001)
        ##        phi.append(0.001)
        ##        shi.append(0.001)
        ##        neus.append([])
        ##        for j in range(0,NumberOfEvents):
        ##            neus[i].append(0.001)

            parameters=[]
            for i in range(0,len(etas)):
                parameters.append(etas[i])
            for i in range(0,len(alphas)):
                parameters.append(alphas[i])
            for i in range(0,len(neus)):
                for j in range(len(neus[i])):
                    parameters.append(neus[i][j])
            for i in range(0,len(ro)):
                parameters.append(ro[i])
            for i in range(0,len(mu)):
                parameters.append(mu[i])
            for i in range(0,len(phi)):
                parameters.append(phi[i])
            for i in range(0,len(shi)):
                parameters.append(shi[i])
                
            
            
            #print LoglikelihoodHawkes(parameters)
            bnds=[]
            for i in range(0,len(etas)):
                bnds.append((0.0001,None))
            for i in range(0,len(alphas)):
                bnds.append((0.0001,None))
            for i in range(0,len(neus)):
                for j in range(len(neus[i])):
                    bnds.append((0.000,None))
            for i in range(0,len(ro)):
                bnds.append((2.001,None))
            for i in range(0,len(mu)):
                bnds.append((0.001,None))
            for i in range(0,len(phi)):
                bnds.append((0.001,None))
            for i in range(0,len(shi)):
                bnds.append((0.001,None))

            ConFunctionList=[]
            for i in range(0,NumberOfEvents):
                def contraintFunc(j):
                    def conFunc(x):
                        return -sum(x[(2+j)*NumberOfEvents:(3+j)*NumberOfEvents])+1
                    return conFunc
                ConFunctionList.append(contraintFunc(i))
            
            cons=()
            for i in range(0,NumberOfEvents):
                cons+={'type': 'ineq', 'fun': ConFunctionList[i]},
            print(cons)


            print('Etas: '+str(etas))
            print('Alphas: '+str(alphas))
            print('Neus: '+str(neus))
            print('Ros: '+str(ro))
            print('Mus: '+str(mu))
            print('Phis: '+str(phi))
            print('Shis: '+str(shi))
            
            try:
                res=minimize(LoglikelihoodHawkes,parameters, method='SLSQP',bounds=bnds,constraints=cons,options = {'maxiter': 100,'disp':True})
                #print(computeIntensity(etas,alphas,neus,ro,mu,phi,shi))
            except:
                traceback.print_exc()
                    
            optimalEtas=[]
            optimalAlphas=[]
            optimalNeus=[]
            optimalRo=[]
            optimalMu=[]
            optimalPhi=[]
            optimalShi=[]

            k=0
            for i in range(0,NumberOfEvents):
                optimalEtas.append(res.x[k])
                k+=1
            for i in range(0,NumberOfEvents):
                optimalAlphas.append(res.x[k])
                k+=1
            for i in range(0,NumberOfEvents):
                optimalNeus.append([])
                for j in range(0,NumberOfEvents):
                    optimalNeus[i].append(res.x[k])
                    k+=1
            for i in range(0,NumberOfEvents):
                optimalRo.append(res.x[k])
                k+=1
            for i in range(0,NumberOfEvents):
                optimalMu.append(res.x[k])
                k+=1
            for i in range(0,NumberOfEvents):
                optimalPhi.append(res.x[k])
                k+=1
            for i in range(0,NumberOfEvents):
                optimalShi.append(res.x[k])
                k+=1

            outputfile.write('iteration: '+str(iteration)+'\n----------------------\n')
            outputfile.write(('Etas: '+str(optimalEtas))+'\n')
            outputfile.write(('Alphas: '+str(optimalAlphas))+'\n')
            outputfile.write(('Neus: '+str(optimalNeus))+'\n')
            outputfile.write(('Ros: '+str(optimalRo))+'\n')
            outputfile.write(('Mus: '+str(optimalMu))+'\n')
            outputfile.write(('Phis: '+str(optimalPhi))+'\n')
            outputfile.write(('Shis: '+str(optimalShi))+'\n')

            outputfile.write('Spectral radius of the branch matrix: '+str(max(abs(numpy.linalg.eigvals(optimalNeus))))+'\n')

            outputfile.write('\n\n\n\n\n\n')
            outputfile.flush()

            
            

        '''
                T=max(points)
                lambdas=[]
                time=[]
                for s in drange2(0,endtime,0.48):
                    time.append(s)
                    lambdas.append(lambda_s(res.x[0],s,points,similarities,res.x[1],res.x[2]))
                  
                    
                plt.figure()
                plt.scatter(points,[0]*len(points))
                plt.plot(time,lambdas)
                plt.title("Hawkes point process Histogram")
                plt.xlabel("Time")
                plt.ylabel("Frequency")
                axes = plt.gca()
                axes.set_xlim([0,endtime])
                plt.savefig(OutputDirectory+'hawkes_file_'+myfile.replace('.hawkesData','')+'_iteration_'+str(i)+'.png')
                plt.close()


        print('lambda_0= '+str(numpy.median(lambda_0)))
        print('alpha= '+str(numpy.median(alpha)))
        print('beta= '+str(numpy.median(beta)))
        '''
          
