import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math
import NDCG


Sections=['Movies','Sports','US','World']
#Sections=['Movies']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))




for section in Sections:
      Relevance={}
      PredictedNDCG=[]
      NaiveNDCG=[]

      
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensityQuerySpecial/'+section+'/'
      ARIMADirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/ARIMAQueryDiff/'+section+'/'
      VARDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VARQuery/'+section+'/'




      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])


      AllQueries={}

      QueryCounts={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        query=line.strip().split(':')[2]
                        if time not in QueryCounts:
                              QueryCounts[time]={}
                        if query in QueryCounts[time]:
                              QueryCounts[time][query]+=1
                        else:
                              QueryCounts[time][query]=1

                        if query not in AllQueries:
                              AllQueries[query]=1
                              
      for time in QueryCounts:
            for query in AllQueries.keys():
                  if query not in QueryCounts[time]:
                        QueryCounts[time][query]=0
                        





      onlyfiles = sorted([f for f in listdir(IntensityDirectory) if isfile(join(IntensityDirectory, f)) and ('.ints_adj' in f)])

      Intensities={}

      for myfile in onlyfiles:
            event=int(myfile.replace('.ints_adj',''))
            with open(IntensityDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        query=line.split(' ## ')[0]
                        intensitiesWithTime=line.split(' ## ')[1].split(',')
                        for insTime in intensitiesWithTime:
                              time=int(float(insTime.split(':')[0]))
                              ints=float(insTime.split(':')[1])
                              if time not in Intensities:
                                    Intensities[time]={}
                              if query not in Intensities[time]:
                                    Intensities[time][query]=ints
                              else:
                                    Intensities[time][query]=ints
                                    




      RankedList={}
      NaiveRankedList={}
      for time in range(TrainingEndTime,int(max(QueryCounts.keys()))+1):
            RankedList[time]=[]
            for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]), reverse=True):
                  RankedList[time].append(mytuple[0])

            NaiveRankedList[time]=[]
            previous_time=time-1
            while previous_time not in QueryCounts:
                  previous_time=previous_time-1
            for mytuple in sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                  NaiveRankedList[time].append(mytuple[0])


      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])


      TotalCount=0
      PredictedInverseRank=0
      NaiveInverseRank=0
      
      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        query=line.strip().split(':')[2]
                        if time<TrainingEndTime:
                              continue

                        TotalCount+=1

                        position=0
                        for q in RankedList[time]:
                              if q.strip().startswith(query.split()[0]):
                                    position+=1
                              if q.strip()==query.strip():
                                    break

                        PredictedInverseRank+=(1/float(position))


                        position=0
                        for q in NaiveRankedList[time]:
                              if q.strip().startswith(query.split()[0]):
                                    position+=1
                              if q.strip()==query.strip():
                                    break

                        NaiveInverseRank+=(1/float(position))
      print('Naive Mean Reciprocal Rank: '+str(NaiveInverseRank/TotalCount)) 
      print('Predicted Mean Reciprocal Rank: '+str(PredictedInverseRank/TotalCount))
      





      #######ARIMA######


      onlyfiles = sorted([f for f in listdir(ARIMADirectory) if isfile(join(ARIMADirectory, f)) and ('.arima' in f)])

      Intensities={}

      for myfile in onlyfiles:
            query=(myfile.replace('.arima',''))
            with open(ARIMADirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        if query not in Intensities[time]:
                              Intensities[time][query]=ints
                        else:
                              Intensities[time][query]=ints
                              




      RankedList={}
      NaiveRankedList={}
      for time in range(TrainingEndTime+1,int(max(QueryCounts.keys()))+1):
            RankedList[time]=[]
            for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]), reverse=True):
                  RankedList[time].append(mytuple[0])

            NaiveRankedList[time]=[]
            previous_time=time-1
            while previous_time not in QueryCounts:
                  previous_time=previous_time-1
            for mytuple in sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                  NaiveRankedList[time].append(mytuple[0])


      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])


      TotalCount=0
      PredictedInverseRank=0
      NaiveInverseRank=0
      
      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        query=line.strip().split(':')[2]
                        if time<=TrainingEndTime:
                              continue

                        TotalCount+=1

                        position=0
                        for q in RankedList[time]:
                              if q.strip().startswith(query.split()[0]):
                                    position+=1
                              if q.strip()==query.strip():
                                    break

                        PredictedInverseRank+=(1/float(position))


                        position=0
                        for q in NaiveRankedList[time]:
                              if q.strip().startswith(query.split()[0]):
                                    position+=1
                              if q.strip()==query.strip():
                                    break

                        NaiveInverseRank+=(1/float(position))
      print('ARIMA Mean Reciprocal Rank: '+str(PredictedInverseRank/TotalCount))
      






      #######VAR######


      onlyfiles = sorted([f for f in listdir(VARDirectory) if isfile(join(VARDirectory, f)) and ('.var' in f)])

      Intensities={}

      for myfile in onlyfiles:
            query=(myfile.replace('.var',''))
            with open(VARDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        if query not in Intensities[time]:
                              Intensities[time][query]=ints
                        else:
                              Intensities[time][query]=ints
                              




      RankedList={}
      NaiveRankedList={}
      for time in range(TrainingEndTime+1,int(max(QueryCounts.keys()))+1):
            RankedList[time]=[]
            for mytuple in sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]), reverse=True):
                  RankedList[time].append(mytuple[0])

            NaiveRankedList[time]=[]
            previous_time=time-1
            while previous_time not in QueryCounts:
                  previous_time=previous_time-1
            for mytuple in sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True):
                  NaiveRankedList[time].append(mytuple[0])


      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])


      TotalCount=0
      PredictedInverseRank=0
      NaiveInverseRank=0
      
      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        query=line.strip().split(':')[2]
                        if time<=TrainingEndTime:
                              continue

                        TotalCount+=1

                        position=0
                        for q in RankedList[time]:
                              if q.strip().startswith(query.split()[0]):
                                    position+=1
                              if q.strip()==query.strip():
                                    break

                        PredictedInverseRank+=(1/float(position))


                        position=0
                        for q in NaiveRankedList[time]:
                              if q.strip().startswith(query.split()[0]):
                                    position+=1
                              if q.strip()==query.strip():
                                    break

                        NaiveInverseRank+=(1/float(position))
      print('VAR Mean Reciprocal Rank: '+str(PredictedInverseRank/TotalCount))
      

