import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math
import random
import numpy

Parameters={}

with open('parametersOptimal.txt','r') as inputfile:
      for line in inputfile:
            if 'section' in line:
                  section=line.strip().split(':')[1].strip()
                  Parameters[section]={}
            elif 'Neus' in line:
                  parameter=line.split(':')[0]
                  values=map(float,line.split(':')[1].split(','))
                  Parameters[section][parameter]=[]
                  for i in range(0,int(math.sqrt(len(values)))):
                        Parameters[section][parameter].append(values[int(i*math.sqrt(len(values))):int(i*math.sqrt(len(values))+math.sqrt(len(values)))])
                  
            elif ',' in line:
                  parameter=line.split(':')[0]
                  values=map(float,line.split(':')[1].split(','))
                  Parameters[section][parameter]=values
            


Summary={}
for section in Parameters:
      Summary[section]={}
      Summary[section]['Etas']=str("{0:.4f}".format(sum(Parameters[section]['Etas'])/len(Parameters[section]['Etas'])))
      Summary[section]['Alphas']=str("{0:.4f}".format(sum(Parameters[section]['Alphas'])/len(Parameters[section]['Alphas'])))
      Summary[section]['Ros']=str("{0:.4f}".format(sum(Parameters[section]['Ros'])/len(Parameters[section]['Ros'])))
      Summary[section]['Mus']=str("{0:.4f}".format(sum(Parameters[section]['Mus'])/len(Parameters[section]['Mus'])))
      Summary[section]['Phis']=str("{0:.4f}".format(sum(Parameters[section]['Phis'])/len(Parameters[section]['Phis'])))
      Summary[section]['Shis']=str("{0:.4f}".format(sum(Parameters[section]['Shis'])/len(Parameters[section]['Shis'])))

      DirectInfluence=[]
      InDirectInfluence=[]
      for i in range(0,10):
            for j in range(0,10):
                  if i==j:
                        DirectInfluence.append(Parameters[section]['Neus'][i][j])
                  else:
                        InDirectInfluence.append(Parameters[section]['Neus'][i][j])
      Summary[section]['Direct']=str("{0:.4f}".format(sum(DirectInfluence)/len(DirectInfluence)))
      Summary[section]['Indirect']=str("{0:.4f}".format(sum(InDirectInfluence)/len(InDirectInfluence)))
      Summary[section]['Spectral']=str("{0:.4f}".format(max(abs(numpy.linalg.eigvals(Parameters[section]['Neus'])))))


      print(section+': '+str(numpy.dot(numpy.linalg.inv(numpy.identity(10)-Parameters[section]['Neus']),Parameters[section]['Etas'])))



mystr='varible'
for section in ['Movies','Sports','US','World']:
      mystr+=','+section
print(mystr)

for variable in sorted(Summary['US']):
      mystr=variable
      for section in ['Movies','Sports','US','World']:
            mystr+=' & '+Summary[section][variable]
      print(mystr)
            

      
      
