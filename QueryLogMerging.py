import os
from os import listdir
from os.path import isfile, join
import shutil
import math
#import Stemming
from stemming.porter2 import stem


QueryLogDirectory='/home/karmake2/InfluenceModeling/Data/QueryLogs/'
EventDirectory='/home/karmake2/InfluenceModeling/Data/FilteredData/'
OutputDirectory='/home/karmake2/InfluenceModeling/Data/BaseData/'

onlyfiles = sorted([f for f in listdir(QueryLogDirectory) if isfile(join(QueryLogDirectory, f)) and ('.query.txt' in f)])



Sections=['Movies','Sports','US','World']


SelectedUniqueQueries={}

with open(EventDirectory+'SelectedQueries.txt') as inputfile:
      for line in inputfile:
            if line.strip() not in SelectedUniqueQueries:
                  SelectedUniqueQueries[line.strip()]=[]


print('Collecting all relevant queries\n#########################\n\n')
for myfile in onlyfiles:
      with open(QueryLogDirectory+myfile) as inputfile:
            print ('processing file: '+myfile)
            for line in inputfile:
                  query=line.strip().split('\t')[3].replace('%20',' ')
                  if query in SelectedUniqueQueries:
                        SelectedUniqueQueries[query].append(int(line.strip().split('\t')[1][:-3]))
                  


try:
      shutil.rmtree(OutputDirectory)
except:
      print('No BaseData folder found')
os.makedirs(OutputDirectory)


print('\n\nGenerating Base Files\n#######################\n\n')
for section in Sections:
      os.makedirs(OutputDirectory+'/'+section)
      print ('\n\nProcessing: '+section+'\n************************\n\n')
      onlyfiles = [f for f in listdir(EventDirectory+'/'+section) if isfile(join(EventDirectory+'/'+section, f)) and ('.csv' in f)]

      for myfile in onlyfiles:
            print('processing file: '+myfile)
            with open (OutputDirectory+section+'/'+myfile,'w') as outputfile:
                  with open (EventDirectory+section+'/'+myfile,'r') as inputfile:
                        while True:
                              myline=inputfile.readline()
                              outputfile.write(myline)
                              if 'Queries' in myline:
                                    outputfile.write(inputfile.readline())
                                    outputfile.write(inputfile.readline())
                                    break

                        eventQueries={}
                        while True:
                              myline=inputfile.readline().strip()
                              segments=myline.split(' #&# ')
                              if myline.strip()=='':
                                    break
                              query=segments[5].replace('%20',' ').strip()
                              if query in eventQueries:
                                    continue
                              eventQueries[query]=1
                              Stemmedquery=segments[0]
                              timestamp=segments[2][:-3]
                              relevnace=segments[1]
                              for timestamp in sorted(SelectedUniqueQueries[query]):
                                    outputfile.write(Stemmedquery+' #&# '+relevnace+' #&# '+str(timestamp)+' #&# '+query+'\n')






                              
