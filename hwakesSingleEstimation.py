import numpy as np
from scipy.optimize import minimize
import math
import numpy
import random
import os
from os import listdir
from os.path import isfile, join
import shutil


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

section='Movies'

ResultDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'




      
MaxSimilarity=0
starttime=1459468800
endtime=(1464739200-starttime)/(60*60)
TrainingEndTime=(1464652800-starttime)/(60*60)



def drange2(start, stop, step):
    numelements = int((stop-start)/float(step))
    for i in range(numelements+1):
            yield start+i*step

            
def lambda_s(lambda_0,s,points,similarities,alpha,beta):
      lambda_s=lambda_0
      for i in range(0,len(points)):
            if points[i] < s:
                  lambda_s+=alpha*similarities[i]*math.exp(-(beta)*(s-points[i]))
      return lambda_s



def CorpusLoglikelihoodHawkes(parameters):
    likelihood=1
    onlyfiles = [f for f in listdir(ResultDirectory) if isfile(join(ResultDirectory, f)) and ('.hawkesData' in f)]

    for myfile in onlyfiles:
        points=[]
        similarities=[]
        with open(ResultDirectory+myfile,'r') as inputfile:
            for line in inputfile:
                time=float(line.split(':')[0])
                if time > TrainingEndTime:
                    break
                points.append(float(line.split(':')[0]))
                similarities.append(float(line.split(':')[1]))
        '''for k in range(0,len(points)):
            points[k]=(points[k]-starttime)/(60*60)'''
        
        likelihood+=LoglikelihoodHawkes(parameters,points,similarities)
    
    print parameters,likelihood
    return -likelihood
            





def LoglikelihoodHawkes(parameters,points,similarities):
    lambda_0=parameters[0]
    alpha=parameters[1]
    beta=parameters[2]


    likelihood=points[len(points)-1]-lambda_0*points[len(points)-1]
    for i in range (0,len(points)):
        likelihood-=(similarities[i])*(alpha/beta)*(1-math.exp(-(beta)*(points[len(points)-1]-points[i])))   
    for i in range(0,len(points)):
        term=lambda_0
        for k in range(max(0,i-50),i):
              term+=alpha*similarities[k]*math.exp(-(beta)*(points[i]-points[k]))
        try:
              likelihood+=math.log(term)
        except:
              print lambda_0,alpha,beta
    return likelihood/1000








lambda_0=[]
alpha=[]
beta=[]
gamma=[]

      


for i in range(0,5):
    print 'iteration: '+str(i)
    #bnds =  ((0.5,5.0), (0.5, 5.0),(0.01,0.5),(1.0,5.0))
    #res = minimize(LoglikelihoodHawkes, [random.uniform(0.5, 5.0),random.uniform(0.5, 5.0),random.uniform(0.01, 0.5),random.uniform(1.0, 5.0)], method='SLSQP', bounds=bnds)

    bnds =  ((0.01,1000), (0.01,1000),(0.01,1000))
    res = minimize(CorpusLoglikelihoodHawkes, [random.uniform(0.001, 10),random.uniform(0.001, 10),random.uniform(0.001,10)], method='SLSQP',bounds=bnds,options = {'maxiter': 2,'disp':True})            

    lambda_0.append(res.x[0])
    alpha.append(res.x[1])
    beta.append(res.x[2])

    print res.x
    

    onlyfiles = [f for f in listdir(ResultDirectory) if isfile(join(ResultDirectory, f)) and ('.hawkesData' in f)]

    
    
    for myfile in onlyfiles:
        points=[]
        similarities=[]
        with open(ResultDirectory+myfile,'r') as inputfile:
            for line in inputfile:
                time=float(line.split(':')[0])
                if time > TrainingEndTime:
                    break
                points.append(float(line.split(':')[0]))
                similarities.append(float(line.split(':')[1]))
        '''for k in range(0,len(points)):
            points[k]=(points[k]-starttime)/(60*60)'''
    

        T=max(points)
        lambdas=[]
        time=[]
        for s in drange2(0,endtime,0.48):
            time.append(s)
            lambdas.append(lambda_s(res.x[0],s,points,similarities,res.x[1],res.x[2]))
          


    print('lambda_0= '+str(numpy.median(lambda_0)))
    print('alpha= '+str(numpy.median(alpha)))
    print('beta= '+str(numpy.median(beta)))

      
