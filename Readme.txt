Datasets:
---------
1. Results.All from Yahoo Intern Repository at timan101.cs.illinois.edu: This is the repository for all events and the associated queries.
2. ProcessedQueryLogs and MoreQueries: This is the repository of all the queries.


One time thing:
----------------
Run QureyLogUnification.py and it will transform MoreQueries data into ProcessedQueryLogs format. This will make all the querylog data into the same format.


The Pipeline:
-------------
1. Run EventSelection.py to select events with highest number of queries. It will create "FilteredData" folder.
2. Run QuerySelection.py to get the list of all relevant queries. It will create "SelectedQueries.txt". 
3. Run QueryLogMerging.py to collect other occurrences of relevant queries over 4 months. It will create "BaseData" Folder.
4. Run HawkesData Preprocess.py to prepare data for the hawkes process modeling. It will create .hawkesData related to each .csv file in BaseData.
5. Then run all the training and testing scripts.