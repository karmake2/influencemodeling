import math
import os
from os import listdir
from os.path import isfile, join
import shutil


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime
import numpy




InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData'
OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/Plots/SimilarityDistribution'

Sections=['World','Movies','US','Sports']



            

try:
      shutil.rmtree(OutputDirectory)
except:
      print ('No Folder Found')
os.makedirs(OutputDirectory)


SimilarityList={}


for section in Sections:
      SimilarityList[section]=[]
      print ('\n\nProcessing: '+section+'\n************************\n\n')
            
      onlyfiles = [f for f in listdir(InputDirectory+'/'+section) if isfile(join(InputDirectory+'/'+section, f)) and ('.hawkesData' in f)]
      #onlyfiles=['0.hawkesData','1.hawkesData']
      for myfile in onlyfiles:
            with open (InputDirectory+'/'+section+'/'+myfile,'r') as inputfile:
                  for line in inputfile:
                        SimilarityList[section].append(float(line.split(':')[1])-1.25)

      print(section+' '+str(sum(SimilarityList[section])/len(SimilarityList[section])+1.25))

      plt.figure()
      n, bins, patches = plt.hist(SimilarityList[section],bins=[0,1,2,3,4,5])                 
      #plt.plot(sorted(QueryRelevance.keys()), similarityList)
      #plt.axvline(newsDay,color='r')
      plt.title('Section: '+section)
      plt.xlabel("textual-similarty")
      plt.ylabel("Frequency")
      axes = plt.gca()
      #axes.set_xlim([0,EndDay])
      #axes.set_ylim([0,50])                  
      plt.savefig(OutputDirectory+'/'+section+'.png')
      plt.close()

