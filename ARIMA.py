
import os
from os import listdir
from os.path import isfile, join
import shutil
import math
from pandas import read_csv
from pandas import datetime
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error



Sections=['Sports','US','World']
#Sections=['Movies']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))

for section in Sections:
      ConflictMemory={}
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/home/karmake2/InfluenceModeling/Data/BaseData/'+section+'/'
      OutputDirectory='/home/karmake2/InfluenceModeling/Results/ARIMA/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)



      Points={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        if time not in Points:
                              Points[time]={}
                        if event in Points[time]:
                              Points[time][event]+=1
                        else:
                              Points[time][event]=1

      for time in range(0,int(max(Points.keys()))+1):
            if time not in Points:
                  Points[time]={}
            for event in range(0,NumberOfEvents):
                  if event not in Points[time]:
                        Points[time][event]=0

      

      
      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True):
            print(event,ChampaionCount[event])'''
      


      ####'time series model'

      TimeSeries={}
      ARIMA_Prediction={}
      
      for event in range(0,NumberOfEvents):
            print('processing event: '+str(event))
            TimeSeries[event]=[]
            with open(OutputDirectory+str(event)+'.arima','w') as outputfile:
                  for time in sorted(Points):
                        TimeSeries[event].append(float(Points[time][event]))
                  train, test = TimeSeries[event][TrainingEndTime-200:TrainingEndTime], TimeSeries[event][TrainingEndTime:len(TimeSeries[event])]
                  history = [x for x in train]
                  predictions = list()
                  counter=1

                  for t in range(len(test)):
                        model = ARIMA(history, order=(5,1,0))
                        try:
                              model_fit = model.fit(disp=0)
                              output = model_fit.forecast()
                              yhat = output[0]
                        except:
                              yhat=0.0
                              print('ARIMA Khelo Bash')
                        
                        predictions.append(yhat)
                        obs = test[t]
                        history.append(obs)
                        outputfile.write(str(int(t+TrainingEndTime))+': '+str(int(round(float(yhat))))+'\n')
                        outputfile.flush()
                        if counter%100==0:
                              print(counter)
                        counter+=1
                                                                     
                                

      
                  
                        
            

      
      
