import numpy
import os
from os import listdir
from os.path import isfile, join
import shutil
import math
from pandas import read_csv
from pandas import datetime
from statsmodels.tsa.api import VAR, DynamicVAR
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
#import matplotlib.pyplot as plt




VARorder=10
Sections=['Movies','Sports','US','World']
#Sections=['Movies']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))




for section in Sections:
      print('processing section: '+str(section)+'  ################\n\n')
      uniqueQueryID=0
      ConflictMemory={}
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensityQuerySpecial/'+section+'/'
      OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VARQuery/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)
      AllQueries={}
      AllQueriesReverseMap={}

      QueryCounts={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        actualTime=float(line.split(':')[0])
                        if actualTime in ConflictMemory:
                              continue
                        else:
                              ConflictMemory[actualTime]=1
                        query=line.strip().split(':')[2]

                        if query not in AllQueries:
                              AllQueries[query]=uniqueQueryID
                              AllQueriesReverseMap[uniqueQueryID]=query
                              uniqueQueryID+=1
                        
                        if time not in QueryCounts:
                              QueryCounts[time]={}
                        
                        if AllQueries[query] in QueryCounts[time]:
                              QueryCounts[time][AllQueries[query]]+=1
                        else:
                              QueryCounts[time][AllQueries[query]]=1

                        


      for time in range(0,int(max(QueryCounts.keys()))+1):
            if time not in QueryCounts:
                  QueryCounts[time]={}
            for query in AllQueries.keys():
                  if AllQueries[query] not in QueryCounts[time]:
                        QueryCounts[time][AllQueries[query]]=0

      

      
      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True):
            print(event,ChampaionCount[event])'''
      


      ####'time series model'

      TimeSeries=[]
      ARIMA_Prediction={}
      train=[]
      test=[]
      
      for query in range(0,uniqueQueryID):
            TimeSeries.append([])
            train.append([])
            test.append([])
            
            for time in sorted(QueryCounts):
                  TimeSeries[query].append(float(QueryCounts[time][query]))
                  train[query] = TimeSeries[query][0:TrainingEndTime]
                  test[query]= TimeSeries[query][TrainingEndTime:len(TimeSeries[query])]

      train=numpy.transpose(train)
      test=numpy.transpose(test)
      TimeSeries=numpy.transpose(TimeSeries)


      try:
            model = VAR(train)
            model_fit = model.fit(VARorder)

            output=[]
            for t in range(len(test)):
                  print('processing time: '+str(t))
                  output.append([])
                  for query in range(0,uniqueQueryID):
                        mysum=model_fit.params[0][event]
                        for i in range(0,VARorder):
                              mysum+=numpy.dot(model_fit.params[i*uniqueQueryID+query+1],TimeSeries[TrainingEndTime+t-i-1])
                        output[t].append(mysum)
      except:
            output=[]
            for t in range(len(test)):
                  print('processing time: '+str(t))
                  output.append([])
                  for query in range(0,uniqueQueryID):
                        mysum=0.0
                        output[t].append(mysum)
            
      
      for query in range(0,uniqueQueryID):
            #print('processing query: '+str(AllQueriesReverseMap[query])+'\n')
            with open(OutputDirectory+str(AllQueriesReverseMap[query])+'.var','w') as outputfile:
                  for t in range(len(test)):
                        outputfile.write(str(int(t+TrainingEndTime))+': '+str(((float(output[t][query]))))+'\n')

            
      
      
                  
                        
            

      
      
