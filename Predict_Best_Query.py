import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math

Sections=['Movies','Sports','US','World']
#Sections=['Movies']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))

for section in Sections:
      ConflictMemory={}
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensityQuerySpecial/'+section+'/'
      ARIMADirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/ARIMAQueryDiff/'+section+'/'
      VARDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VARQuery/'+section+'/'

      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)
      AllQueries={}

      QueryCounts={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        actualTime=float(line.split(':')[0])
                        if actualTime in ConflictMemory:
                              continue
                        else:
                              ConflictMemory[actualTime]=1
                        query=line.strip().split(':')[2]
                        if time not in QueryCounts:
                              QueryCounts[time]={}
                        if query in QueryCounts[time]:
                              QueryCounts[time][query]+=1
                        else:
                              QueryCounts[time][query]=1

                        if query not in AllQueries:
                              AllQueries[query]=1

      for time in QueryCounts:
            for query in AllQueries.keys():
                  if query not in QueryCounts[time]:
                        QueryCounts[time][query]=0
                        
      onlyfiles = sorted([f for f in listdir(IntensityDirectory) if isfile(join(IntensityDirectory, f)) and ('.ints_adj' in f)])
      Intensities={}

      for myfile in onlyfiles:
            event=int(myfile.replace('.ints_adj',''))
            with open(IntensityDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        query=line.split(' ## ')[0]
                        intensitiesWithTime=line.split(' ## ')[1].split(',')
                        for insTime in intensitiesWithTime:
                              time=int(float(insTime.split(':')[0]))
                              ints=float(insTime.split(':')[1])
                              if time not in Intensities:
                                    Intensities[time]={}
                              if query not in Intensities[time]:
                                    Intensities[time][query]=ints
                              else:
                                    #print('paisi')
                                    Intensities[time][query]+=ints


      
      
      

      
      

      
      Correct=0
      Incorrect=0
      IncorrectList=[]

      NaiveCorrect=0
      NaiveIncorrect=0

      ChampaionCount={}
      
      for time in range(TrainingEndTime,int(max(QueryCounts.keys()))):
            if time in QueryCounts:
                  Predicted_Champaion=sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]), reverse=True)[0][0]
                  Actual_Champion=sorted(QueryCounts[time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]
                  
                  previous_time=time-1
                  while previous_time not in QueryCounts:
                        previous_time=previous_time-1
                  Previous_Champion=sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]

                  if Predicted_Champaion==Actual_Champion:
                        Correct+=1
                        #print(str(time)+' ### '+Actual_Champion+': '+str(QueryCounts[time][Actual_Champion])+'('+str(Intensities[time-1][Actual_Champion])+')'+'\t'+Predicted_Champaion+': '+str(QueryCounts[time][Predicted_Champaion])+'('+str(Intensities[time-1][Predicted_Champaion])+')')
                  else:
                        Incorrect+=1
                        IncorrectList.append(time)
                        
                  if Previous_Champion==Actual_Champion:
                        NaiveCorrect+=1
                        if Predicted_Champaion!=Actual_Champion:
                              #print(str(time)+' ### '+Actual_Champion+': '+str(QueryCounts[time][Actual_Champion])+'('+str(Intensities[time-1][Actual_Champion])+')'+'\t'+Predicted_Champaion+': '+str(QueryCounts[time][Predicted_Champaion])+'('+str(Intensities[time-1][Predicted_Champaion])+')')
                              if (Predicted_Champaion,Actual_Champion) in ChampaionCount:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]+=1
                              else:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]=1
                  else:
                        NaiveIncorrect+=1

      print('Naive Accuracy: '+str(float(NaiveCorrect)/(NaiveCorrect+NaiveIncorrect)))
      print(NaiveCorrect,NaiveIncorrect)
      print('Accuracy: '+str(float(Correct)/(Correct+Incorrect)))
      print(Correct,Incorrect)
      #print(IncorrectList)
      

      
 


      onlyfiles = sorted([f for f in listdir(ARIMADirectory) if isfile(join(ARIMADirectory, f)) and ('.arima' in f)])
      Intensities={}

      for myfile in onlyfiles:
            query=(myfile.replace('.arima',''))
            with open(ARIMADirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        if query not in Intensities[time]:
                              Intensities[time][query]=ints
                        else:
                              #print('paisi')
                              Intensities[time][query]+=ints


      
      
      
      '''time=200
      print('Predicted Order\n-------------------------')
      for query in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True)[:10]:
            print(query,Intensities[time-1][query])
      print('\n\nActual Order\n-------------------------')
      for query in sorted(QueryCounts[time],key=QueryCounts[time].get,reverse=True)[:10]:
            print(query,QueryCounts[time][query])'''
      
      

      
      Correct=0
      Incorrect=0
      IncorrectList=[]

      NaiveCorrect=0
      NaiveIncorrect=0

      ChampaionCount={}
      
      for time in range(TrainingEndTime+1,int(max(QueryCounts.keys()))):
            if time in QueryCounts:
                  Predicted_Champaion=sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]), reverse=True)[0][0]
                  Actual_Champion=sorted(QueryCounts[time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]
                  
                  previous_time=time-1
                  while previous_time not in QueryCounts:
                        previous_time=previous_time-1
                  Previous_Champion=sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]

                  if Predicted_Champaion==Actual_Champion:
                        Correct+=1
                        #print(str(time)+' ### '+Actual_Champion+': '+str(QueryCounts[time][Actual_Champion])+'('+str(Intensities[time-1][Actual_Champion])+')'+'\t'+Predicted_Champaion+': '+str(QueryCounts[time][Predicted_Champaion])+'('+str(Intensities[time-1][Predicted_Champaion])+')')
                  else:
                        Incorrect+=1
                        IncorrectList.append(time)
                        
                  if Previous_Champion==Actual_Champion:
                        NaiveCorrect+=1
                        if Predicted_Champaion!=Actual_Champion:
                              #print(str(time)+' ### '+Actual_Champion+': '+str(QueryCounts[time][Actual_Champion])+'('+str(Intensities[time-1][Actual_Champion])+')'+'\t'+Predicted_Champaion+': '+str(QueryCounts[time][Predicted_Champaion])+'('+str(Intensities[time-1][Predicted_Champaion])+')')
                              if (Predicted_Champaion,Actual_Champion) in ChampaionCount:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]+=1
                              else:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]=1
                  else:
                        NaiveIncorrect+=1

      print('ARIMA Accuracy: '+str(float(Correct)/(Correct+Incorrect)))
      print(Correct,Incorrect)
      #print(IncorrectList)
      

      
      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True)[:10]:
            print(event,ChampaionCount[event])'''


      onlyfiles = sorted([f for f in listdir(VARDirectory) if isfile(join(VARDirectory, f)) and ('.var' in f)])
      Intensities={}

      for myfile in onlyfiles:
            query=(myfile.replace('.var',''))
            with open(VARDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        if query not in Intensities[time]:
                              Intensities[time][query]=ints
                        else:
                              #print('paisi')
                              Intensities[time][query]+=ints
                              
      Correct=0
      Incorrect=0
      IncorrectList=[]

      NaiveCorrect=0
      NaiveIncorrect=0

      ChampaionCount={}
      
      for time in range(TrainingEndTime+1,int(max(QueryCounts.keys()))):
            if time in QueryCounts:
                  Predicted_Champaion=sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]), reverse=True)[0][0]
                  Actual_Champion=sorted(QueryCounts[time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]
                  
                  previous_time=time-1
                  while previous_time not in QueryCounts:
                        previous_time=previous_time-1
                  Previous_Champion=sorted(QueryCounts[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]

                  if Predicted_Champaion==Actual_Champion:
                        Correct+=1
                        #print(str(time)+' ### '+Actual_Champion+': '+str(QueryCounts[time][Actual_Champion])+'('+str(Intensities[time-1][Actual_Champion])+')'+'\t'+Predicted_Champaion+': '+str(QueryCounts[time][Predicted_Champaion])+'('+str(Intensities[time-1][Predicted_Champaion])+')')
                  else:
                        Incorrect+=1
                        IncorrectList.append(time)
                        #print(str(time)+' ### '+Actual_Champion+': '+str(QueryCounts[time][Actual_Champion])+'('+str(Intensities[time-1][Actual_Champion])+')'+'\t'+Predicted_Champaion+': '+str(QueryCounts[time][Predicted_Champaion])+'('+str(Intensities[time-1][Predicted_Champaion])+')')
                              
                        
                  if Previous_Champion==Actual_Champion:
                        NaiveCorrect+=1
                        if Predicted_Champaion!=Actual_Champion:
                              #print(str(time)+' ### '+Actual_Champion+': '+str(QueryCounts[time][Actual_Champion])+'('+str(Intensities[time-1][Actual_Champion])+')'+'\t'+Predicted_Champaion+': '+str(QueryCounts[time][Predicted_Champaion])+'('+str(Intensities[time-1][Predicted_Champaion])+')')
                              if (Predicted_Champaion,Actual_Champion) in ChampaionCount:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]+=1
                              else:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]=1
                  else:
                        NaiveIncorrect+=1

      print('VAR Accuracy: '+str(float(Correct)/(Correct+Incorrect)))
      print(Correct,Incorrect)
      #print(IncorrectList)

      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True)[:10]:
            print(event,ChampaionCount[event])'''
      





      
      
