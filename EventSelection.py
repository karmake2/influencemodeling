import math
import os
from os import listdir
from os.path import isfile, join
import shutil




AllDataDirectory='/home/karmake2/YahooIntern2016/Results.All/Correlation/NewYorkTimes'
OutputDirectory='/home/karmake2/InfluenceModeling/Data/FilteredData'

Sections=['Movies','Sports','US','World']           
#Sections=['World']            


try:
      shutil.rmtree(OutputDirectory)
except:
      print ('No Folder Found')
os.makedirs(OutputDirectory)


            
           
for section in Sections:
      os.makedirs(OutputDirectory+'/'+section)
      queryCounts={}
      print ('\n\nProcessing: '+section+'\n************************\n\n')      
      onlyfiles = [f for f in listdir(AllDataDirectory+'/'+section) if isfile(join(AllDataDirectory+'/'+section, f)) and ('.csv' in f)]
      for myfile in onlyfiles:
            #print('Reading file: '+myfile)
            
            with open (AllDataDirectory+'/'+section+'/'+myfile,'r') as inputfile:
                  inputfile.readline()
                  inputfile.readline()
                  newstimestamp=int(inputfile.readline())
                  
                  while True:
                        myline=inputfile.readline()
                        if 'Queries' in myline:
                              inputfile.readline()
                              inputfile.readline()
                              break
                  
                  count=0
                  while True:
                        myline=inputfile.readline()
                        segments=myline.split(' #&# ')
                        if myline.strip()=='':
                              break
                        
                        if float(segments[1]) < 1.25:
                              break
                        count+=1
                  queryCounts[myfile]=count
                  
      eventCounter=0
      for myfile in sorted(queryCounts,key=queryCounts.get,reverse=True)[:40]:
            print('processing file: '+myfile+' '+str(queryCounts[myfile]))
            with open (OutputDirectory+'/'+section+'/'+str(eventCounter)+'.csv','w') as outputfile:
                  with open (AllDataDirectory+'/'+section+'/'+myfile,'r') as inputfile:
                        eventCounter+=1
                        while True:
                              myline=inputfile.readline()
                              outputfile.write(myline)
                              if 'Queries' in myline:
                                    outputfile.write(inputfile.readline())
                                    outputfile.write(inputfile.readline())
                                    break
                  
                        while True:
                              myline=inputfile.readline()
                              segments=myline.split(' #&# ')
                              if myline.strip()=='':
                                    break
                              if float(segments[1]) < 1.25:
                                    break
                              outputfile.write(myline)





