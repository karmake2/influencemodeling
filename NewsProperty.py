import math
import os
from os import listdir
from os.path import isfile, join
import shutil





Sections=['Movies','Sports','US','World']           
           
InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'
           
for section in Sections:
      print ('\n\nProcessing: '+section+'\n************************\n\n')      
      onlyfiles = [f for f in listdir(InputDirectory+'/'+section) if isfile(join(InputDirectory+'/'+section, f)) and ('.csv' in f)]

      TitleLength=[]
      BodyLength=[]
      
      for myfile in onlyfiles:
            with open (InputDirectory+'/'+section+'/'+myfile,'r') as inputfile:
                  inputfile.readline()
                  inputfile.readline()
                  inputfile.readline()
                  inputfile.readline()
                  news=(inputfile.readline())
                  TitleLength.append(len(news.split('advertisement advertisement')[0].split()))
                  BodyLength.append(len(news.split('advertisement advertisement')[1].split()))
      print('Title: '+str(float(sum(TitleLength))/len(TitleLength)))
      print('Body: '+str(float(sum(BodyLength))/len(BodyLength)))
                  
                  
