from __future__ import print_function


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math
import random

Parameters={}
zeta=0.8

with open('parametersSpecial.txt','r') as inputfile:
      for line in inputfile:
            if 'section' in line:
                  section=line.strip().split(':')[1].strip()
                  Parameters[section]={}
            elif 'Neus' in line:
                  parameter=line.split(':')[0]
                  values=map(float,line.split(':')[1].split(','))
                  Parameters[section][parameter]=[]
                  for i in range(0,int(math.sqrt(len(values)))):
                        Parameters[section][parameter].append(values[int(i*math.sqrt(len(values))):int(i*math.sqrt(len(values))+math.sqrt(len(values)))])
                  
            elif ',' in line:
                  parameter=line.split(':')[0]
                  values=map(float,line.split(':')[1].split(','))
                  Parameters[section][parameter]=values
            
'''myCOu=1
for line in Parameters['Movies']['Neus']:
      print('\t',end='')
      print(myCOu,end='')
      print(' & ', end='')
      myCOu+=1
      for i in range(len(line)):
            print("{0:.6f}".format(line[i]), end='')
            if i < len(line)-1:
                  print(' & ', end='')
      print('\\\\\\hline')'''
      
duplicates={}
for section in Parameters:
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensityQuerySpecial/'+section+'/'
      PriorDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/Prior/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])


      EventQueries={}
      Points={}
      NumberOfEvents=len(onlyfiles)
      Queries={}
      
      
      for myfile in onlyfiles:
            EventQueries[int(myfile.replace('.hawkesData',''))]={}
            with open(InputDirectory+myfile,'r') as inputfile:
                  #print('Processing event: '+str(myfile.replace('.hawkesData','')))
                  for line in inputfile:
                        time=float(line.split(':')[0])
                        actualTime=float(line.split(':')[0])

                        if time in Points:
                              continue
                        '''if time in Points:
                              #print('Problem')
                              if section in duplicates:
                                    duplicates[section]+=1
                              else:
                                    duplicates[section]=1
                              while time in Points:
                                    #print(myfile.replace('.hawkesData','')+': '+str(time)+': '+ line.split(':')[2].strip())
                                    noise=random.uniform(0.01,0.02)
                                    time=actualTime+noise'''
                        
                        similarity=float(line.split(':')[1])
                        query=line.split(':')[2].strip()
                        EventQueries[int(myfile.replace('.hawkesData',''))][query]=similarity
                        Points[time]={}
                        Points[time]['event']=int(myfile.replace('.hawkesData',''))
                        Points[time]['query']=query
                        Points[time]['similarity']=similarity-1.25
                        if query not in Queries:
                              Queries[query]=int(myfile.replace('.hawkesData',''))




      onlyfiles = sorted([f for f in listdir(PriorDirectory) if isfile(join(PriorDirectory, f)) and ('.prior' in f)])
      Priors={}
      
      
      for myfile in onlyfiles:
            event=int(myfile.replace('.prior',''))
            Priors[event]={}
            with open(PriorDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        query=line.split(':')[0]
                        pr=float(line.split(':')[1])
                        Priors[event][query]=pr

      
      Etas=Parameters[section]['Etas']
      alphas=Parameters[section]['Alphas']
      neus=Parameters[section]['Neus']
      ro=Parameters[section]['Ros']
      mu=Parameters[section]['Mus']
      phi=Parameters[section]['Phis']
      shi=Parameters[section]['Shis']

      QueryEtas=[]
      for event in range(len(Etas)):
            QueryEtas.append({})
            for query in EventQueries[event]:
                  if query not in Priors[event]:
                        Priors[event][query]=0.0000001
                  QueryEtas[event][query]=Etas[event]*Priors[event][query]
                                                                           
      NumberOfEvents=len(Etas)

      PointList=sorted(Points.keys())
      
      lambdas=[]
      for event in range(len(Etas)):
            lambdas.append([])
            for x in range(len(PointList)):
                  lambdas[event].append({})
                  for q in EventQueries[event]:
                        lambdas[event][x][q]=QueryEtas[event][q]


      previousEventDict={}
      for i in range(1,len(PointList)):
            if i%1000==0:
                  print('Processed: '+str(i))
            previousEvent=Points[PointList[i-1]]['event']
            previousMark=(Points[PointList[i-1]]['similarity']*shi[previousEvent]+phi[previousEvent])*(float((ro[previousEvent]-1)*(ro[previousEvent]-2))/((phi[previousEvent]*(ro[previousEvent]-1)*(ro[previousEvent]-2)+shi[previousEvent]*mu[previousEvent]*(ro[previousEvent]-2))))
            for j in range(0,len(Etas)):
                  for query in lambdas[j][i]:
                        lambdas[j][i][query]=QueryEtas[j][query]+math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*(lambdas[j][i-1][query]-QueryEtas[j][query])+neus[j][previousEvent]*Priors[j][query]*alphas[j]*math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*previousMark
                        if previousEvent==j:
                              if Points[PointList[i-1]]['query']==query:
                                    lambdas[j][i][query]=QueryEtas[j][query]+math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*(lambdas[j][i-1][query]-QueryEtas[j][query])+neus[j][previousEvent]*zeta*alphas[j]*math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*previousMark
                              else:
                                    lambdas[j][i][query]=QueryEtas[j][query]+math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*(lambdas[j][i-1][query]-QueryEtas[j][query])+neus[j][previousEvent]*((1-zeta)/(len(lambdas[j][i])-1))*alphas[j]*math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*previousMark


      for j in range(0,NumberOfEvents):
            with open(OutputDirectory+str(j)+'.ints','w') as outputfile:
                  for query in EventQueries[j]:
                        outputfile.write(query+' ## ')
                        for i in range(0,len(PointList)):
                              outputfile.write(str(lambdas[j][i][query]))
                              if i<len(PointList)-1:
                                    outputfile.write(',')
                        outputfile.write('\n')

      
      AdjustedLambdas=[[{} for x in range(0,int(max(Points.keys())+1))] for y in range(len(Etas))] 

      print('\n\nComputing AdjustedLambdas\n\n')
      for i in range(10,int(math.floor(max(Points.keys())))):
            for k in range(0,len(PointList)):
                  if PointList[k]>i:
                        NearestPoint=PointList[k-1]
                        break
            for j in range(0,len(Etas)):
                  if k==0:
                        for query in lambdas[j][i]:
                              AdjustedLambdas[j][i][query]=QueryEtas[j][query]
                  else:
                        for query in lambdas[j][i]:
                              AdjustedLambdas[j][i][query]=QueryEtas[j][query]+math.exp(-alphas[j]*(i-NearestPoint))*(lambdas[j][k-1][query]-QueryEtas[j][query])

      print('\n\nWriting AdjustedLambdas\n\n')
      FinalPoint=int(math.floor(max(Points.keys())))
      for j in range(0,NumberOfEvents):
            print('Processed event: '+str(j))
            with open(OutputDirectory+str(j)+'.ints_adj','w') as outputfile:
                  for query in EventQueries[j]:
                        outputfile.write(query+' ## ')
                        for i in range(10,FinalPoint):
                              outputfile.write(str(i)+':'+str(AdjustedLambdas[j][i][query]))
                              if i< FinalPoint-1:
                                    outputfile.write(',')
                        outputfile.write('\n')






                        
