import os
from os import listdir
from os.path import isfile, join
import shutil

starttime=1459486800
TrainingEndTime=(1464652800-starttime)/(60*60)


for section in ['Movies','Sports','US','World']:
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/Prior/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])


      EventQueries={}
      
      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            EventQueries[event]={}
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=float(line.split(':')[0])
                        if time > TrainingEndTime:
                            break
                        query=line.split(':')[2].strip()
                        if query in EventQueries[event]:
                              EventQueries[event][query]+=1
                        else:
                              EventQueries[event][query]=1


      for myfile in onlyfiles:
            with open(OutputDirectory+myfile.replace('.hawkesData','.prior'),'w') as outputfile:
                  event=int(myfile.replace('.hawkesData',''))
                  TotalQuery=float(sum(EventQueries[event].values()))
                  for query in EventQueries[event]:
                        outputfile.write(query+':'+str(EventQueries[event][query]/TotalQuery)+'\n')
                        




                        
