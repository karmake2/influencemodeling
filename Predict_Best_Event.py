import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math



Sections=['Movies','Sports','US','World']
#Sections=['US']
starttime=1459486800
TrainingEndTime=int((1464652800-starttime)/(60*60))

for section in Sections:
      ConflictMemory={}
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      IntensityDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensitySpecial/'+section+'/'
      ARIMADirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/ARIMA/'+section+'/'
      VARDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/VAR/'+section+'/'



      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)],reverse=True)



      Points={}
      NumberOfEvents=len(onlyfiles)

      for myfile in onlyfiles:
            event=int(myfile.replace('.hawkesData',''))
            with open(InputDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        if time not in Points:
                              Points[time]={}
                        if event in Points[time]:
                              Points[time][event]+=1
                        else:
                              Points[time][event]=1

      for time in Points:
            for event in range(0,NumberOfEvents):
                  if event not in Points[time]:
                        Points[time][event]=0
                        
      onlyfiles = sorted([f for f in listdir(IntensityDirectory) if isfile(join(IntensityDirectory, f)) and ('.ints_adj' in f)])

      Intensities={}

      for myfile in onlyfiles:
            event=int(myfile.replace('.ints_adj',''))
            with open(IntensityDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        ints=float(line.split(':')[1])
                        if time not in Intensities:
                              Intensities[time]={}
                        Intensities[time][event]=ints
      '''
      time=69
      print('Predicted Order\n-------------------------')
      for event in sorted(Intensities[time-1],key=Intensities[time-1].get,reverse=True):
            print(event,Intensities[time-1][event])
      print('\n\nActual Order\n-------------------------')
      for event in sorted(Points[time],key=Points[time].get,reverse=True):
            print(event,Points[time][event])
      
      '''
      for time in range(10,int(max(Points.keys()))):
            if time not in Intensities:
                  Intensities[time]=Intensities[time-1]
      
      
      Correct=0
      Incorrect=0
      IncorrectList=[]

      NaiveCorrect=0
      NaiveIncorrect=0

      ChampaionCount={}
      
      for time in range(TrainingEndTime,int(max(Points.keys()))):
            if time in Points:
                  Predicted_Champaion=sorted(Intensities[time-1].items(),key=lambda x: (x[1],x[0]), reverse=True)[0][0]
                  Actual_Champion=sorted(Points[time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]
                  
                  previous_time=time-1
                  while previous_time not in Points:
                        previous_time=previous_time-1
                  Previous_Champion=sorted(Points[previous_time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]

                  if Predicted_Champaion==Actual_Champion:
                        Correct+=1
                  else:
                        Incorrect+=1
                        IncorrectList.append(time)

                  if Previous_Champion==Actual_Champion:
                        NaiveCorrect+=1
                        if Predicted_Champaion!=Actual_Champion:
                              if (Predicted_Champaion,Actual_Champion) in ChampaionCount:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]+=1
                              else:
                                    ChampaionCount[(Predicted_Champaion,Actual_Champion)]=1
                  else:
                        NaiveIncorrect+=1
      
      print('Naive Accuracy: '+str(float(NaiveCorrect)/(NaiveCorrect+NaiveIncorrect)))
      print(NaiveCorrect,NaiveIncorrect)
      print('Accuracy: '+str(float(Correct)/(Correct+Incorrect)))
      print(Correct,Incorrect)
      #print(IncorrectList)
      

      
      '''for event in sorted(ChampaionCount,key=ChampaionCount.get,reverse=True):
            print(event,ChampaionCount[event])'''
      


      ####'time series model'

      ARIMA_Prediction={}
      onlyfiles = sorted([f for f in listdir(ARIMADirectory) if isfile(join(ARIMADirectory, f)) and ('.arima' in f)])


      for myfile in onlyfiles:
            event=int(myfile.replace('.arima',''))
            with open(ARIMADirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        frequency=float(line.split(':')[1])
                        if time not in ARIMA_Prediction:
                              ARIMA_Prediction[time]={}
                        ARIMA_Prediction[time][event]=frequency

                        
      Correct=0
      Incorrect=0
      for time in range(TrainingEndTime,int(max(Points.keys()))):
            if time in Points:
                  Actual_Champion=sorted(Points[time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]
                  Predicted_Champaion=sorted(ARIMA_Prediction[time].items(),key=lambda x: (x[1],x[0]), reverse=True)[0][0]
                  if Predicted_Champaion==Actual_Champion:
                        Correct+=1
                  else:
                        Incorrect+=1

      print('ARIMA Accuracy: '+str(float(Correct)/(Correct+Incorrect)))
      print(Correct,Incorrect)


      ####'VAR model'
      ARIMA_Prediction={}
      onlyfiles = sorted([f for f in listdir(VARDirectory) if isfile(join(VARDirectory, f)) and ('.var' in f)])


      for myfile in onlyfiles:
            event=int(myfile.replace('.var',''))
            with open(VARDirectory+myfile,'r') as inputfile:
                  for line in inputfile:
                        time=int(float(line.split(':')[0]))
                        frequency=float(line.split(':')[1])
                        if time not in ARIMA_Prediction:
                              ARIMA_Prediction[time]={}
                        ARIMA_Prediction[time][event]=frequency

                        
      Correct=0
      Incorrect=0
      for time in range(TrainingEndTime,int(max(Points.keys()))):
            if time in Points:
                  Actual_Champion=sorted(Points[time].items(),key=lambda x: (x[1],x[0]),reverse=True)[0][0]
                  Predicted_Champaion=sorted(ARIMA_Prediction[time].items(),key=lambda x: (x[1],x[0]), reverse=True)[0][0]
                  if Predicted_Champaion==Actual_Champion:
                        Correct+=1
                  else:
                        Incorrect+=1
                        #print(Predicted_Champaion,Actual_Champion)

      print('VAR Accuracy: '+str(float(Correct)/(Correct+Incorrect)))
      print(Correct,Incorrect)


                  
                        
            

      
      
