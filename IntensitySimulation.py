import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import shutil
import math
import random

Parameters={}

with open('parametersSpecial.txt','r') as inputfile:
      for line in inputfile:
            if 'section' in line:
                  section=line.strip().split(':')[1].strip()
                  Parameters[section]={}
            elif 'Neus' in line:
                  parameter=line.split(':')[0]
                  values=map(float,line.split(':')[1].split(','))
                  Parameters[section][parameter]=[]
                  for i in range(0,int(math.sqrt(len(values)))):
                        Parameters[section][parameter].append(values[int(i*math.sqrt(len(values))):int(i*math.sqrt(len(values))+math.sqrt(len(values)))])
                  
            elif ',' in line:
                  parameter=line.split(':')[0]
                  values=map(float,line.split(':')[1].split(','))
                  Parameters[section][parameter]=values
            



for section in ['Movies']:
      duplicates={}
      print('\n\nprocessing section: '+section+'\n--------------------------\n')
      InputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/BaseData/'+section+'/'
      OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Results/IntensitySpecial/'+section+'/'

      try:
            shutil.rmtree(OutputDirectory)
      except:
            print ('No Folder Found')
      os.makedirs(OutputDirectory)
      
      onlyfiles = sorted([f for f in listdir(InputDirectory) if isfile(join(InputDirectory, f)) and ('.hawkesData' in f)])


      EventPoints={}
      Points={}
      NumberOfEvents=len(onlyfiles)

      
      for myfile in onlyfiles:
            EventPoints[myfile.replace('.hawkesData','')]={}
            with open(InputDirectory+myfile,'r') as inputfile:
                  #print('Processing event: '+str(myfile.replace('.hawkesData','')))
                  for line in inputfile:
                        time=float(line.split(':')[0])
                        query=line.split(':')[2].strip()
                        event=myfile.replace('.hawkesData','')

                        if str(time) not in duplicates:
                              duplicates[str(time)]=[]
                        duplicates[str(time)].append(event+':'+query)
                  
                        if time in Points:
                              continue
                              
                        '''if time in Points:
                              while time in Points:
                                    #print(myfile.replace('.hawkesData','')+': '+str(time)+': '+ line.split(':')[2].strip())
                                    noise=random.uniform(0.01,0.011)
                                    time=actualTime+noise'''

                        
                        
                        similarity=float(line.split(':')[1])
                        query=line.split(':')[2].strip()
                        EventPoints[myfile.replace('.hawkesData','')][time]={}
                        EventPoints[myfile.replace('.hawkesData','')][time]['query']=query
                        EventPoints[myfile.replace('.hawkesData','')][time]['similarity']=similarity-1.25
                        Points[time]={}
                        Points[time]['event']=int(myfile.replace('.hawkesData',''))
                        Points[time]['query']=query
                        Points[time]['similarity']=similarity-1.25


      etas=Parameters[section]['Etas']
      alphas=Parameters[section]['Alphas']
      neus=Parameters[section]['Neus']
      ro=Parameters[section]['Ros']
      mu=Parameters[section]['Mus']
      phi=Parameters[section]['Phis']
      shi=Parameters[section]['Shis']

                                                                           
      NumberOfEvents=len(etas)

      PointList=sorted(Points.keys())
      
      lambdas=[[0 for x in range(len(PointList))] for y in range(len(etas))]
      for event in range(NumberOfEvents):
            lambdas[event][0]=etas[event]

      previousEventDict={}
      for i in range(1,len(PointList)):
            previousEvent=Points[PointList[i-1]]['event']
            previousMark=(Points[PointList[i-1]]['similarity']*shi[previousEvent]+phi[previousEvent])*(float((ro[previousEvent]-1)*(ro[previousEvent]-2))/((phi[previousEvent]*(ro[previousEvent]-1)*(ro[previousEvent]-2)+shi[previousEvent]*mu[previousEvent]*(ro[previousEvent]-2))))
            for j in range(0,len(etas)):
                  lambdas[j][i]=etas[j]+math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*(lambdas[j][i-1]-etas[j])+neus[j][previousEvent]*alphas[j]*math.exp(-alphas[j]*(PointList[i]-PointList[i-1]))*previousMark
            if previousEvent in previousEventDict:
                  previousEventDict[previousEvent]+=1
            else:
                  previousEventDict[previousEvent]=1
      
      AdjustedLambdas=[[0 for x in range(0,int(max(Points.keys())+1))] for y in range(len(etas))]
      
      for i in range(10,int(math.floor(max(Points.keys())))):
            for k in range(0,len(PointList)):
                  if PointList[k]>i:
                        NearestPoint=PointList[k-1]
                        break
            for j in range(0,len(etas)):
                  if k==0:
                        AdjustedLambdas[j][i]=etas[j]
                  else:
                        AdjustedLambdas[j][i]=etas[j]+math.exp(-alphas[j]*(i-NearestPoint))*(lambdas[j][k-1]-etas[j])
      
      
      for j in range(0,NumberOfEvents):
            plt.figure()
            plt.plot(PointList,lambdas[j])
            #plt.axvline(newsDay,color='r')
            plt.title('Event No: '+str(j))
            plt.xlabel("Time (in Hours)")
            plt.ylabel("Intensity of the event")
            axes = plt.gca()
            axes.set_xlim([0,max(PointList)])
            #axes.set_ylim([0,50])
            
            
            plt.savefig(OutputDirectory+'/'+str(j)+'.png')
            print('processed event: ' + str(j))
            plt.close()

      
      for j in range(0,NumberOfEvents):
            with open(OutputDirectory+str(j)+'.ints_adj','w') as outputfile:
                  for i in range(10,int(math.floor(max(Points.keys())))):
                        outputfile.write(str(i)+':'+str(AdjustedLambdas[j][i])+'\n')
      
      
      for j in range(0,NumberOfEvents):
            with open(OutputDirectory+str(j)+'.ints','w') as outputfile:
                  for i in range(0,len(PointList)):
                        outputfile.write(str(PointList[i])+':'+str(lambdas[j][i])+'\n')
      


      jhamela=0
      total=0
      for mykey in sorted(duplicates):
            total+=1
            if len(list(set(duplicates[mykey])))>1:
                  #print(mykey+':'+str(list(set(duplicates[mykey]))))
                  jhamela+=1
      print(total,jhamela,float(jhamela)/total)

                        
