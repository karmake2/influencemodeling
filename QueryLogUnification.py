import os
from os import listdir
from os.path import isfile, join
import shutil
import math
#import Stemming
from stemming.porter2 import stem



mypath='/Users/shubhrakanti/Documents/FutureTextPrediction/RawDataJune/'
OutputDirectory='/Users/shubhrakanti/Documents/InfluenceModeling/Data/QueryLogs/'
IDFOutputDirectory="/Users/shubhrakanti/Documents/InfluenceModeling/Data/IDF/"

##mypath='/home/karmake2/YahooIntern2016/Datasets/MoreQueries/'
##OutputDirectory='/home/karmake2/InfluenceModeling/Data/QueryLogs/'
##IDFOutputDirectory='/home/karmake2/InfluenceModeling/Data/IDF/'


onlyfiles = sorted([f for f in listdir(mypath) if isfile(join(mypath, f)) and ('.query.txt' in f)])

shutil.rmtree(OutputDirectory)
os.makedirs(OutputDirectory)


IDF={}
Stopwords=[]

with open('stop-word-list.txt','r') as inputfile:
      for line in inputfile:
            Stopwords.append(line.strip().lower())
            
TotalQueries=0
TotalWords=0
for myfile in onlyfiles:
      i=0
      QueryLogALL={}
      QueryLogTime={}
      with open(mypath+myfile,'r') as inputfile:
            for myline in inputfile:
                  line=myline.lower()
                  QuerySentence=''
                  words=line.split('\t')[2].replace('%20',' ').split()
                  
                  nonEnglisgFlag=False
                  for word in words:
                        if '%' in word or '\\' in word:
                              nonEnglisgFlag=True
                              break
                        QuerySentence+=word.strip()+' '

                  if nonEnglisgFlag==True:
                        continue

                  '''
                  
                  if list(set(Companies).intersection(set(words)))==[]:
                        continue
                  '''


                  QueryLogTime[i]=line.split()[1]
                  TotalWords+=len(words)
                  
                  #StemmedQuery=str(Stemming.perform_Stemming(QuerySentence.strip()))
                  StemmedQuery=QuerySentence.strip()

                  for char in ["u\"","u'","'",'[',']',"\"",':','-','.','&',',']:
                        StemmedQuery=StemmedQuery.replace(char,' ')

                  processedQuery=''
                  for word in StemmedQuery.split():
                        if word not in Stopwords and stem(word) not in Stopwords and len(word)>1:
                              processedQuery+=stem(word)+' '

                                    
                  for word in set(processedQuery.split()):
                        if word not in Stopwords:
                              if word.strip() in IDF:
                                    IDF[word.strip()]+=1
                              else:
                                    IDF[word.strip()]=1
                                    
                  QueryLogALL[i]=(line.split('\t')[0]+'\t'+line.split('\t')[1]+'\t'+processedQuery.strip().replace(' ','%20')+'\t'+line.split('\t')[2].replace(' ','%20'))
                  i+=1
                  if i%100==0:
                        break
                  #print line.split()[2]+'\n'+processedQuery.strip()+'\n\n\n\n'
      TotalQueries+=i
      


      with open(OutputDirectory+myfile,'w') as outputfile:
            for i in sorted(QueryLogTime, key=QueryLogTime.get):
                  outputfile.write(QueryLogALL[i].strip()+'\n')
      print 'processed file: '+ myfile



for word in IDF:
      IDF[word]=math.log((TotalQueries-IDF[word]+0.5)/(IDF[word]+0.5))

with open(IDFOutputDirectory+'IDF.txt','w') as outputfile:
      for word in sorted(IDF, key=IDF.get, reverse=True):
            outputfile.write(word+'  &&  '+str(IDF[word])+'\n')


with open(IDFOutputDirectory+'AverageQueryLength.txt','w') as outputfile:
      outputfile.write(str(float(TotalWords)/float(TotalQueries)))







